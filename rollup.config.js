import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import legacy from 'rollup-plugin-legacy';

export default {
  //external: ['Waypoint'],
 
  input: 'src/js/index.js',
  output: {
    file: 'src/js/bundle.js',
    name: 'catholicPropertyInvestigation',
    format: 'iife',
    sourcemap: true,
    sourcemapFile: 'src/js/bundle.js.map',
    globals: {
      //'Waypoint': 'Waypoint'
    }
  },
  plugins: [
    legacy({
      // add a default export, corresponding to `someLibrary`
      './src/js/lib/noframework.waypoints.min.js': 'Waypoint'
    }),
    resolve({
      // use "module" field for ES6 module if possible
      module: true, // Default: true

      // use "jsnext:main" if possible
      // – see https://github.com/rollup/rollup/wiki/jsnext:main
      jsnext: true, // Default: false

      // use "main" field or index.js, even if it's not an ES6 module
      // (needs to be converted from CommonJS to ES6
      // – see https://github.com/rollup/rollup-plugin-commonjs
      main: true, // Default: true

      // some package.json files have a `browser` field which
      // specifies alternative files to load for people bundling
      // for the browser. If that's you, use this option, otherwise
      // pkg.browser will be ignored
      browser: false, // Default: false

      // not all files you want to resolve are .js files
      extensions: ['.js'], // Default: ['.js']

      // whether to prefer built-in modules (e.g. `fs`, `path`) or
      // local ones with the same names
      preferBuiltins: false // Default: true

      // Lock the module search in this path (like a chroot). Module defined
      // outside this path will be mark has external
      //jail: '/my/jail/path', // Default: '/'

      // If true, inspect resolved files to check that they are
      // ES2015 modules
      // modulesOnly: true, // Default: false

      // Any additional options that should be passed through
      // to node-resolve
      //customResolveOptions: {
      //  moduleDirectory: 'js_modules'
      //}
    }),
    commonjs({
      namedExports: {
          'node_modules/jquery/dist/jquery.min.js': [ 'jquery' ]
          //'node_modules/waypoints/lib/noframework.waypoints': ['Waypoint']
          //  'node_modules/d3/d3.js' : ['d3'],
          // 'node_modules/bootstrap/dist/js/bootstrap.min.js' : ['bootstrap']
      }
  })
  ]
};
