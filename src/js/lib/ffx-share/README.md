# [Fairfax Share Library](#markdown-header-fairfax-share-library)

The **Fairfax Share Library** is a **collection of tools for setting up social media share buttons**. A reworking of the share utilities originally written by Nathanael Scott, it is intended for use in the production of features, interactives and more by the company's Presentation Development team.

The fact that it's a reworking is important - it's pretty far from perfect, but does all that the old script did more cleanly.

The library has no dependencies and - this might pose issues for some, but can be easily changed - is written to be imported using the [Rollup module bundler](https://rollupjs.org/).

## Contents

* [API](#markdown-header-api)
    * [Setup metadata](#markdown-header-setup-metadata)
    * [Setup Facebook buttons](#markdown-header-setup-facebook-buttons)
    * [Setup Twitter buttons](#markdown-header-setup-twitter-buttons)
    * [Setup Google buttons](#markdown-header-setup-google-buttons)
    * [Setup WhatsApp buttons](#markdown-header-setup-whatsapp-buttons)
    * [Setup Email buttons](#markdown-header-setup-email-buttons)
* [Standard metadata properties expected](#markdown-header-standard-metadata-properties-expected)


## [API](#markdown-header-api)

Below is a description of the library's key functions.

In summary, the application can be used like so:

```
import * as ffxShare from "./ffx-share"

function setupSharing() {

  ffxShare.setupMetadata({
    title: "VCE Honour Roll 2017: Search the state's top students!",
    description: 
      "With exams now behind them, it's time to celebrate Victoria's top performers! Find out " +
      "if any students you know scored 40 or more in a subject.",
    url: u.getShareUrl()
  });

  ffxShare.setupTwitterButtons("social-button__twitter");
  ffxShare.setupFacebookButtons("social-button__facebook");
  ffxShare.setupEmailButtons("social-button__email");
  ffxShare.setupWhatsAppButtons("social-button__whatsapp");
}

setupSharing();
```

### [Setup metadata](#markdown-header-setup-metadata) [^](#markdown-header-fairfax-share-library)

`setupMetadata(metadata)`

| Parameter name | Data type | Description | Options | Default | Required |
| -------------- | --------- | ----------- | ------- | ------- | -------- |
| metadata       | Object    | Object bearing social media metadata properties, with a minimum of `title`, `description` and `url` (all strings) | NA | NA | Yes |

Function sets up the page's social media sharing metadata.

### [Setup Facebook buttons](#markdown-header-setup-facebook-buttons) [^](#markdown-header-fairfax-share-library)

`setupFacebookButtons(facebookButtonClass)`

| Parameter name | Data type | Description | Options | Default | Required |
| -------------- | --------- | ----------- | ------- | ------- | -------- |
| facebookButtonClass | String    | Name of class applied to all Facebook share buttons | NA | NA | Yes |

Functions sets up the page's Facebook share buttons.

### [Setup Twitter buttons](#markdown-header-setup-twitter-buttons) [^](#markdown-header-fairfax-share-library)

`setupTwitterButtons(twitterButtonClass)`

| Parameter name | Data type | Description | Options | Default | Required |
| -------------- | --------- | ----------- | ------- | ------- | -------- |
| twitterButtonClass | String    | Name of class applied to all Twitter share buttons | NA | NA | Yes |

Functions sets up the page's Twitter share buttons.

### [Setup Google buttons](#markdown-header-setup-google-buttons) [^](#markdown-header-fairfax-share-library)

`setupGoogleButtons(googleButtonClass)`

| Parameter name | Data type | Description | Options | Default | Required |
| -------------- | --------- | ----------- | ------- | ------- | -------- |
| googleButtonClass | String    | Name of class applied to all Google share buttons | NA | NA | Yes |

Functions sets up the page's Google share buttons.

### [Setup WhatsApp buttons](#markdown-header-setup-whatsapp-buttons) [^](#markdown-header-fairfax-share-library)

`setupWhatsAppButtons(whatsAppButtonClass)`

| Parameter name | Data type | Description | Options | Default | Required |
| -------------- | --------- | ----------- | ------- | ------- | -------- |
| whatsAppButtonClass | String    | Name of class applied to all WhatsApp share buttons | NA | NA | Yes |

Functions sets up the page's WhatsApp share buttons.

### [Setup Email buttons](#markdown-header-setup-email-buttons) [^](#markdown-header-fairfax-share-library)

`setupEmailButtons(emailButtonClass)`

| Parameter name | Data type | Description | Options | Default | Required |
| -------------- | --------- | ----------- | ------- | ------- | -------- |
| emailButtonClass | String    | Name of class applied to all Email share buttons | NA | NA | Yes |

Functions sets up the page's Email share buttons.

## [Standard metadata properties expected](#markdown-header-standard-metadata-properties-expected)

The library expects the following metadata properties to be present. (It could probably be rewritten not to require these, but this dependency is a legacy of previous implementations; social media sharing has been this kind of weird gray fog that I haven't wanted to mess with too much, so just add these tags to the `head` tag in your HTML.)

```
<!-- Schema tags -->
<link rel="canonical" href="">
<link rel="standout" href="">
<meta name="description" content="">
<meta name="news_keywords" content="">
<meta itemprop="name" content="Some title name">
<meta itemprop="description" content="">
<meta itemprop="image" content="">
<meta itemprop="author" name="author" content="Some author">

<!-- Facebook tags -->
<meta property="fb:app_id" content="230318243699378">
<meta property="og:title" content="Some title name">
<meta property="og:type" content="Article">
<meta property="og:url" content="">
<meta property="og:image" content="">
<meta property="og:description" content="">
<meta property="og:site_name" content="The Age">
<meta property="og:caption" content="">

<!-- Twitter tags -->
<meta name="twitter:card" content="">
<meta name="twitter:site" content="@theage"> 
<meta name="twitter:title" content="Some title name">  
<meta name="twitter:description" content="">  
<meta name="twitter:creator" content="">  
<meta name="twitter:image" content="">  
<meta name="twitter:url" content=""> 
```