/*
  Fairfax Share 

  January 2, 2017
  Ask sorenfrederiksen@fairfaxmedia.com.au

  This is a reworking of the share utilities originally written by Nathaneal Scott. It has been
  redesigned to work without external libraries, like jQuery.
*/

var share = function() {
  var domain = window.location.host;

  var facebookAppId = {
    'smh.com.au': '193677504039845',
    'canberratimes.com.au': '106020559517129',
    'brisbanetimes.com.au': '107082829404370',
    'theage.com.au': '230318243699378',
    'watoday.com.au': '254346064617808',
    localhost: '230318243699378' // Testing default is The Age
  };

  var siteNames = {
    theage: 'The Age',
    smh: 'The Sydney Morning Herald',
    brisbanetimes: 'The Brisbane Times',
    watoday: 'WA Today',
    canberratimes: 'The Canberra Times'
  };

  var twitterHandles = {
    'smh.com.au': 'smh',
    'theage.com.au': 'theage',
    'watoday.com.au': 'watoday',
    'brisbanetimes.com.au': 'brisbanetimes',
    'canberratimes.com.au': 'canberratimes',
    'goodfood.com.au': 'goodfood',
    'dailylife.com.au': 'dailylife',
    'domain.com.au': 'domain',
    localhost: 'theage' // Testing default is The Age
  };

  //
  // Metadata
  // -----------------------------------------------------------------------------

  /*
    Sharing metadata - Set up those parts of the site's sharing metadata that aren't affected
    by the user's results.
  */

  function setupSharingMetadata(metadata) {
    if (
      !metadata.hasOwnProperty('title') ||
      !metadata.hasOwnProperty('description') ||
      !metadata.hasOwnProperty('url')
    ) {
      console.error(
        '[ERR] Setup sharing metadata: Required values missing. Sharing metadata ' +
          'requires title, description and url properties, at minimum.'
      );
    }

    // *-> Facebook
    document.head.querySelector('[property=\'fb:app_id\']').content = u.getFacebookAppId(domain);
    document.head.querySelector('[property=\'og:title\']').content = metadata.title;
    document.head.querySelector('[property=\'og:url\']').content = metadata.url || u.getCurrentPageUrl();
    document.head.querySelector('[property=\'og:description\']').content = metadata.description;
    document.head.querySelector('[property=\'og:site_name\']').content = u.getSiteName(domain);

    // *-> Twitter
    document.head.querySelector('[name=\'twitter:site\']').content = u.getTwitterHandle(domain);
    document.head.querySelector('[name=\'twitter:title\']').content = metadata.title;
    document.head.querySelector('[name=\'twitter:description\']').content = metadata.description;
    document.head.querySelector('[name=\'twitter:creator\']').content = u.getTwitterHandle(domain);
    document.head.querySelector('[name=\'twitter:url\']').content =metadata.url || u.getCurrentPageUrl();

    // *-> Schema
    document.head.querySelector('[name=\'description\']').content = metadata.description;
    document.head.querySelector('[name=\'news_keywords\']').content = 'Australia, Census, 2016, Demographics';
    document.head.querySelector('[itemprop=\'name\']').content = metadata.description;
    document.head.querySelector('[itemprop=\'description\']').content = metadata.description;
    document.head.querySelector('[itemprop=\'author\']').content = 'Soren Frederiksen and Craig Butt';

    // *-> Fairfax
    document.head.querySelector('[rel=\'canonical\']').href = metadata.url || u.getCurrentPageUrl();
    document.head.querySelector('[rel=\'standout\']').href = metadata.url || u.getCurrentPageUrl();
  }

  //
  // Facebook
  // -----------------------------------------------------------------------------

  /*
    Setup facebook share buttons

    @param { String } facebookButtonClass - Class for Facebook share buttons.
  */

  function setupFacebookShareButtons(facebookButtonClass) {
    if (!facebookButtonClass) {
      facebookButtonClass = 'social-facebook-btn';
    }

    /*
      Handle click on Facebook share button
    */

    function handleClickFacebookShareButton(event) {
      event.preventDefault();

      FB.ui(
        {
          method: 'share',
          quote: document.head.querySelector('[property=\'og:caption\']').content,
          href: document.head.querySelector('[property=\'og:url\']').content
        },
        function(response) {}
      );

      return false;
    }

    function setupFacebookSharingLibrary() {
      window.fbAsyncInit = function() {
        //window.fbAPI = FB;

        FB.init({
          appId: u.getFacebookAppId(domain),
          xfbml: true,
          version: 'v2.3'
        });
      };

      (function(d, s, id) {
        var js,
          fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
          return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = '//connect.facebook.net/en_US/sdk.js';
        fjs.parentNode.insertBefore(js, fjs);
      })(document, 'script', 'facebook-jssdk');
    }

    // Wire up Facebook share button handler
    setupFacebookSharingLibrary();
    var facebookButtons = document.getElementsByClassName(facebookButtonClass);

    for (var i = 0; i < facebookButtons.length; i++) {
      facebookButtons[i].addEventListener(
        'click',
        handleClickFacebookShareButton,
        false
      );
    }
  }

  //
  // Twitter
  // -----------------------------------------------------------------------------

  /*
    Setup Twitter share buttons

    @param { String } twitterShareButtonClass - Class for Twitter share buttons.
  */

  function setupTwitterShareButtons(twitterShareButtonClass) {
    if (!twitterShareButtonClass) {
      twitterShareButtonClass = 'social-twitter-btn';
    }

    /*
      Handle click on social Twitter share button
    */

    function handleClickTwitterShareButton(event) {
      event.preventDefault();

      var url = document.head.querySelector('[name=\'twitter:url\']').content;
      var description = document.head.querySelector('[name=\'twitter:description\']').content;
      var twitterHandle = document.head.querySelector('[name=\'twitter:site\']').content;

      window.open(
        'https://twitter.com/share?url=' +
          encodeURIComponent(url) +
          '&text=' +
          encodeURIComponent(description) +
          '&via=' +
          twitterHandle,
        'twitterShareDialog',
        'width=626,height=436,scrollbars=1'
      );

      return false;
    }

    // Wire up Twitter share button handler
    var twitterButtons = document.getElementsByClassName(
      twitterShareButtonClass
    );

    for (var i = 0; i < twitterButtons.length; i++) {
      twitterButtons[i].addEventListener(
        'click',
        handleClickTwitterShareButton,
        false
      );
    }
  }

  //
  // Google
  // -----------------------------------------------------------------------------

  /*
    Setup Google share buttons

    @param { String } googleShareButtonClass - Class for Google share buttons.
  */

  function setupGoogleShareButtons(googleShareButtonClass) {
    if (!googleShareButtonClass) {
      googleShareButtonClass = 'social-google-btn';
    }

    /*
      Handle click on social Google Plus share button
    */

    function handleClickGooglePlusShareButton(event) {
      event.preventDefault();

      var url = document.head.querySelector('[property=\'og:url\']').content;

      window.open(
        'https://plus.google.com/share?url=' + encodeURIComponent(url),
        'googlePlusShareDialog',
        'width=626,height=436,scrollbars=1'
      );

      return false;
    }

    // Wire up Google Plus share button handler
    var googlePlusButtons = document.getElementsByClassName(
      googleShareButtonClass
    );

    for (var i = 0; i < googlePlusButtons.length; i++) {
      googlePlusButtons[i].addEventListener(
        'click',
        handleClickGooglePlusShareButton,
        false
      );
    }
  }

  //
  // Reddit
  // -----------------------------------------------------------------------------

  /*
    Setup Reddit share buttons

    @param { String } redditShareButtonClass - Class for Reddit share buttons.
  */

  function setupRedditShareButtons(redditShareButtonClass) {
    if (!redditShareButtonClass) {
      redditShareButtonClass = 'social-reddit-btn';
    }

    /*
      Handle click on social Reddit share button
    */

    function handleClickRedditShareButton(event) {
      event.preventDefault();

      var url = document.head.querySelector('[property=\'og:url\']').content;

      window.open(
        'https://www.reddit.com/submit?url=' + encodeURIComponent(url),
        'redditShareDialog',
        'width=626,height=436,scrollbars=1'
      );

      return false;
    }

    // Wire up Reddit share button handler
    var redditButtons = document.getElementsByClassName(redditShareButtonClass);

    for (var i = 0; i < redditButtons.length; i++) {
      redditButtons[i].addEventListener(
        'click',
        handleClickRedditShareButton,
        false
      );
    }
  }

  //
  // Email
  // -----------------------------------------------------------------------------

  /*
    Setup Email share buttons

    @param { String } emailShareButtonClass - Class for Email share buttons.
  */

  function setupEmailShareButtons(emailShareButtonClass) {
    if (!emailShareButtonClass) {
      emailShareButtonClass = 'social-email-btn';
    }

    /*
      Handle click on social Email share button
    */

    function handleClickEmailShareButton(event) {
      event.preventDefault();

      var url = document.head.querySelector('[property=\'og:url\']').content;

      var title = encodeURIComponent(
        document.head.querySelector('[property=\'og:title\']').content
      );
      var description = encodeURIComponent(
        document.head.querySelector('[property=\'og:description\']').content
      );
      var body = description + '%20' + encodeURIComponent(url);

      window.open('mailto:?subject=' + title + '&body=' + body);
      return false;
    }

    // Wire up Email share button handler
    var emailButtons = document.getElementsByClassName(emailShareButtonClass);

    for (var i = 0; i < emailButtons.length; i++) {
      emailButtons[i].addEventListener(
        'click',
        handleClickEmailShareButton,
        false
      );
    }
  }

  //
  // WhatsApp
  // -----------------------------------------------------------------------------

  /*
    Setup WhatsApp share buttons

    @param { String } whatsAppButtonClass - Class for WhatsApp share buttons.
  */
  function setupWhatsAppButtons(whatsAppButtonClass) {

    if (!whatsAppButtonClass) {
      whatsAppButtonClass = 'social-whatsapp-btn';
    }

    /*
      Handle click on social Email share button
    */

    function getWhatsAppShareMessageHref() {

      var url = document.head.querySelector('[property=\'og:url\']').content;

      var title = encodeURIComponent(
        document.head.querySelector('[property=\'og:title\']').content
      );

      var description = encodeURIComponent(
        document.head.querySelector('[property=\'og:description\']').content
      );

      var shareMessage = title + ' | ' + description + ' - ' + url;
      var shareMessageHref = 'whatsapp://send?text=' + shareMessage;

      return shareMessageHref;
    }

    // Wire up Email share button handler
    var whatsAppButtons = document.getElementsByClassName(whatsAppButtonClass);
    var shareHref = getWhatsAppShareMessageHref();

    for (var i = 0; i < whatsAppButtons.length; i++) {
      var link = whatsAppButtons[i].querySelector('a');
      link.href = shareHref;
    }

  }

  //
  // Utilities
  // -----------------------------------------------------------------------------

  var u = {
    /*
      Returns the Facebook App ID of a masthead, based on an input domain.

      @param { String } domain - A given domain.
    */

    getFacebookAppId: function(domain) {
      for (var d in facebookAppId) {
        if (domain && domain.indexOf(d) >= 0) {
          return facebookAppId[d];
        }
      }
      return null;
    },

    /*
      Returns the Twitter handle of a masthead, based on an input domain.

      @param { String } domain - A given domain.
    */

    getTwitterHandle: function(domain) {
      for (var d in twitterHandles) {
        if (domain && domain.indexOf(d) >= 0) {
          return twitterHandles[d];
        }
      }
      return null;
    },

    /*
      Returns the current URL of the page.
    */

    getCurrentPageUrl: function() {
      var url = (window.location !== window.parent.location) ? document.referrer: document.location.href;
      return url;
    },

    /*
      Returns the site name associated with a given domain.

      @param { String } domain - A given domain.
    */

    getSiteName: function(domain) {
      for (var name in siteNames) {
        if (domain && domain.indexOf(name) >= 0) {
          return siteNames[name];
        }
      }
      return 'The Sydney Morning Herald';
    }
  };

  //
  // Export
  // -----------------------------------------------------------------------------

  return {
    setupMetadata: setupSharingMetadata,
    setupEmailButtons: setupEmailShareButtons,
    setupRedditButtons: setupRedditShareButtons,
    setupGoogleButtons: setupGoogleShareButtons,
    setupFacebookButtons: setupFacebookShareButtons,
    setupTwitterButtons: setupTwitterShareButtons,
    setupWhatsAppButtons: setupWhatsAppButtons
  };
};

var shareTools = share();

export default shareTools;
