#!/bin/sh

#
# Reminify
#
# January 24, 2018
# Ask sorenfrederiksen@fairfaxmedia.com.au if issues arise
#
# Script to make the minification of the messenger script easily replicable.
#
# The script is expecting uglify-js version '3.2.2'. To install this version exactly, use the
# following command:
#
#   npm install -g uglify-js@3.2.2
#
# For more, see:
#
#   https://github.com/mishoo/UglifyJS2

uglifyjs ffx-iframe-messenger.js -c -m -o ffx-iframe-messenger.min.js --source-map "filename='ffx-iframe-messenger.min.js.map',root='/',url='ffx-iframe-messenger.min.js.map'"