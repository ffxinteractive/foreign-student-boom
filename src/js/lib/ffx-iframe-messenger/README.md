# [Fairfax Iframe Messenger Library](#markdown-header-fairfax-iframe-messenger-library)

The **Fairfax Iframe Messenger Library** is a script that facilitates communication between an iframe and its parent. At this point, its principle purpose is to help the parent resize its iframes according to the size of the documents they enclose.

The script does this in accordance with the dynamic iframe height message specifications [outlined here](https://ffxblue.atlassian.net/wiki/spaces/TECH/pages/223608833/Dynamic+iframe+height).

The library is setup to be imported as a either a CommonJS (`module.exports = ...` etc.) or AMD (`define(...)` etc.) module, or - if it detects neither of these options - it will be attached to the environment's global object (i.e. the `window` object, in browsers).

## Contents

* [API](#markdown-header-api)
    * [Enable auto-resize](#markdown-header-enable-auto-resize)
    * [Resize](#markdown-header-resize)
* [Credits](#markdown-header-credits)

## [API](#markdown-header-api)

### [Enable auto-resize](#markdown-header-enable-auto-resize)

`iframeMessenger.enableAutoResize(options)`

Function that has the iframe 'watch' the size of the document it encloses, automatically sending resize messages to the iframe's parent whenever its size changes.

**Most of the time, your experience of this library will be executing this function in your code once, then just letting it do its thing.**

(The [original documents](https://github.com/guardian/iframe-messenger) mention an `absoluteHeight` property that can be set on the `options` object, described below. In this version of the library, no such option is available.)

| Parameter name | Data type | Description | Options | Default | Required |
| -------------- | --------- | ----------- | ------- | ------- | -------- |
| options           | Object    | Object specifying minor modifications to the auto-resizing function | the `enableUpdateInterval` property of this object can be set to `true` or `false` | If not specified, the `enableUpdateInterval` is set to `true` | No |

For example:

```

/* In the iframed document's code */

iframeMessenger.enableAutoResize(); /* The iframe will now sent 'resize' messages automatically */

```

### [Resize](#markdown-header-resize)

`iframeMessenger.resize(height)`

Function that sends a message to the iframe's parent, asking it to resize the iframe.

| Parameter name | Data type | Description | Options | Default | Required |
| -------------- | --------- | ----------- | ------- | ------- | -------- |
| height           | Number or String    | Integer denoting a pixel height or a string denoting a percentage height that will be used to resize the iframe | e.g. 10, '20%' | If not specified, the current size of the iframed document will be sent | No |

For example:

```

/* In the iframed document's code */

iframeMessenger.resize(200); /* Resize iframe to 200px

iframeMessenger.resize('25%'); /* Resize iframe to 25% of the height of its container */

iframeMessenger.resize(); /* Resize iframe to size of the document it encloses (most common use-case) */

```

## [Credits](#markdown-header-credits)

The library is heavily based on the `iframe-messenger` script open-sourced by The Guardian's digital team. The original library can be found [here](http://www.apache.org/licenses/LICENSE-2.0).

The original library included functionality of no use to Fairfax at this point. Related code has been removed and/or commented out, so as to be removed in minification. (The reason some of this functionality persists in the comments is that it may prove of use in future, and so it was thought to make sense to keep it on hand.)