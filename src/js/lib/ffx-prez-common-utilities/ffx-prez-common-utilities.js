/*
  Fairfax Presentation Common Utilities

  January 2, 2017
  ask sorenfrederiksen@fairfaxmedia.com.au

  A series of general purpose utilities for use across Fairfax Presentation interactives.

  Unfortunately, the utilities fall short of being general purpose, as a few depend on the D3 data 
  visualisation library, in particular its `d3-format` module. (By default, these dependent 
  utilities are commented out, so as to avoid bundling errors.)
*/

//
// D3-dependent utilities
// -----------------------------------------------------------------------------

//import * as d3 from '../../d3-imports';

/*
  Format number

  Returns the number rendered as a string according to Fairfax style, or close to it.

  @param { Number } number - Number to be formatted
*/


//ar numberFormatter = d3.format(",.1f");

function formatNumber(number) {
  return numberFormatter(number).replace(/.0$/, "");
}


//
// Dependency-free utilities
// -----------------------------------------------------------------------------

/*
  Get current page URL

  Returns the URL of the page in which the interactive is embedded or, failing that, the URL for
  the interactive itself.
*/

function getCurrentPageUrl() {
  return (window.location !== window.parent.location) ? document.referrer : document.location.href;
}

/*
  Get URL argument values

  Returns an object containing each of the provided URL parameters as keys and each of the 
  associated values as values.
*/

function getUrlArgumentValues() {
  var urlArguments = location.search.substring(1).split('&');
  var argumentSet;
  var urlArgumentsAndValues = {};

  if (urlArguments.length > 0) {
    for (var i = 0; i < urlArguments.length; i++) {
      argumentSet = urlArguments[i].split('=');

      if (argumentSet.length >= 2) {
        urlArgumentsAndValues[argumentSet[0]] = argumentSet[1];
      }
    }
  }
  return urlArgumentsAndValues;
}

/*
  Make readable event time

  Returns a rendering of the input timestamp that 'makes sense', given the current time. (For 
  example, if the timestamp denotes some time earlier in the same day, the timestamp might be
  rendered as just '6pm'; a timestamp denoting the same time a few days before might just read
  'November 6'.)

  @param { String } utcTimestamp - A timestamp denoting the time to be made readable
*/

function makeReadableEventTime(utcTimestamp) {

  var date = new Date(utcTimestamp);
  var now = new Date();

  // If same year 
  if (now.getFullYear() === date.getFullYear()) {

    // ... and month ...
    if (now.getMonth() === date.getMonth()) {

      // ... and same date, do time(e.g. '6pm')
      if (now.getDate() === date.getDate()) {
        return getTime(date.getHours(), date.getMinutes());
      }
      // ... and yesterday, do time and note (e.g. '6pm yesterday')
      else if (now.getDate() - 1 === date.getDate()) {
        return getTime(date.getHours(), date.getMinutes()) + " yesterday";
      }
      // Otherwise, do date (e.g. 'November 4')
      else {
        return getMonthName(date.getMonth()) + " " + date.getDate();
      }
    }
    // Otherwise, do date (e.g. 'November 6')
    else {
      return getMonthName(date.getMonth()) + " " + date.getDate();
    }
  }
  // Otherwise, do date and year (e.g. 'November 6, 2017')
  else {
    return getMonthName(date.getMonth()) + " " + date.getDate() + ", " + date.getFullYear();
  }

  function getMonthName(monthNumber) {
    var monthNames = [
      "January", 
      "February", 
      "March", 
      "April", 
      "May", 
      "June",
      "July", 
      "August", 
      "September", 
      "October", 
      "November", 
      "December"
    ];
    return monthNames[monthNumber];
  }

  function getTime(hour, minutes) {
    if (hour < 13) {
      return hour + ":" + (minutes > 9 ? minutes : "0" + minutes) + (hour === 12 ? "pm" : "am");
    }
    else {
      return (hour % 12) + ":" + (minutes > 9 ? minutes : "0" + minutes) + "pm";
    }
  }
}

/*
  Is mobile

  Returns boolean indicating whether or not the user appears to be on a mobile device.
*/

function isMobile() {
  return document.body.clientWidth < 600;
}

/*
  Make short name

  Returns shortened version of a given name, with all but the person's last name initialised.

  @param { String } name - Name of a person to be shortened
*/

function makeShortName(name) {

  var shortName = "";
  var names = name.split(" ");

  // Make this transformation:
  //  Jason Akermanis  -> J. Akermanis
  //  Chris L. Johnson -> C. L. Johnson
  for (var i = 0; i < names.length; i++) {
    if (i === names.length - 1) {
      shortName += " " + names[i];
    }
    else {
      shortName += names[i][0] + ".";
    }
  }

  return shortName;
}

//
// Export
// -----------------------------------------------------------------------------

var utilities = {
  formatNumber: formatNumber,
  getCurrentPageUrl: getCurrentPageUrl,
  getUrlArgumentValues: getUrlArgumentValues,
  makeReadableEventTime: makeReadableEventTime,
  isMobile: isMobile,
  makeShortName: makeShortName
};

export default utilities;