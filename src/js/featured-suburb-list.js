/*
  Featured suburb list
*/

import * as ffx from './ffx-imports';
// import * as d3 from './d3-imports';

function featuredSuburbList(userSettings) {
  var settings, state, data, dispatcher;

  //
  // Setup
  // -----------------------------------------------------------------------------

  var setup = {

    /*
      Setup settings
    */
    settings: function() {

      // Default settings
      settings = {
        suburbListId: '#property-gallery',
        suburbDetailsId: '#property-gallery__suburb-details'
      };

      // Override defaults, where the user has input their own settings
      var userSettingsKeys = Object.keys(userSettings);

      for (var i = 0; i < userSettingsKeys.length; i++) {
        settings[userSettingsKeys[i]] = userSettings[userSettingsKeys[i]];
      }
    },

    /*
      Setup state
    */
    state: function() {

      var initialWindowSize = window.innerWidth < 600 ? 'small' : window.innerWidth < 768 ? 'medium' : 'large';

      state = {
        windowSize: initialWindowSize, // or 'small', 'medium'
        suburbList: d3.select(settings.suburbListId),
        suburbDetails: d3.select(settings.suburbDetailsId),
        suburbDetailsRows: null,
        suburbInFocus: null,
        suburbInFocusOrderPosition: -1,
        suburbsData: null
      };
    },

    /*
      Setup data
    */
    data: function() {

      d3.csv('src/data/top-10-suburbs-properties.csv', process, function(error, data) {
        if (error) {
          dispatcher.call('error__thrown', this, error);
        }
        else {
          dispatcher.call('data__loaded', this, data);
        }
      });

      function process(d) {
        d.value = +d.value;
        return d;
      }
    },

    /*
      Setup dispatcher
    */
    dispatcher: function() {
      dispatcher = d3.dispatch(
        'user__changed-suburb-in-focus',
        'user__closes-suburb-details',
        'window__changed-to-new-size',
        'data__loaded',
        'error__thrown'
      );
    },

    /*
      Setup dispatcher events
    */
    dispatcherEvents: function() {

      /*
        When data is loaded
      */
      dispatcher.on('data__loaded', function(data) {
        state.suburbsData = data;
      });

      /*
        When user changes from one suburb to another
        
        @param { String } selectedSuburb - String denoting the suburb the user has selected
        @param { D3 selection } selectedSuburbElement - D3 selection of element representing suburb
        in the featured suburb list
      */
      dispatcher.on('user__changed-suburb-in-focus', function(selectedSuburb, selectedSuburbElement) {

        state.suburbInFocus = selectedSuburb;

        // Draw suburb details
        var selectedSuburbData = state.suburbsData.filter(function(d) {
          return d.suburb === state.suburbInFocus;
        })
        .sort(function(a, b) {
          return a.value > b.value ? -1 : a.value === b.value ? 0 : 1; 
        });

        draw.suburbDetails(selectedSuburbData);

        // Set suburb element into selected state
        d3.selectAll('.featured-suburb-list__suburb').classed('is--in-focus', false);
        selectedSuburbElement.classed('is--in-focus', true);

        // Position suburb details element
        state.suburbInFocusOrderPosition = +selectedSuburbElement.style('order');
        var suburbDetailsPosition = u.getSuburbDetailsOrderPosition(state.suburbInFocusOrderPosition, state.windowSize)
        var suburbDetailsFocus = u.getSuburbDetailsFocusPosition(state.suburbInFocusOrderPosition, state.windowSize);

        state.suburbDetails.style('order', suburbDetailsPosition);
        u.setSuburbDetailsFocusClass(suburbDetailsFocus);
      });

      /*
        Close suburb details
      */
      dispatcher.on('user__closes-suburb-details', function() {
        state.suburbInFocus = null;
        state.suburbDetails.classed('is--open', false);
        d3.selectAll('.featured-suburb-list__suburb').classed('is--in-focus', false);
      });

      /*
        When window changes to a new size range

        @param { String } newSize - String describing size of the user's viewport (either 'small',
        'medium' or 'large')
      */
      dispatcher.on('window__changed-to-new-size', function(newSize) {

        state.windowSize = newSize;

        if (state.suburbInFocus !== null) {
          var detailsPosition = u.getSuburbDetailsOrderPosition(state.suburbInFocusOrderPosition, state.windowSize)
          var detailsFocusPosition = u.getSuburbDetailsFocusPosition(state.suburbInFocusOrderPosition, state.windowSize);

          state.suburbDetails.style('order', detailsPosition);
          u.setSuburbDetailsFocusClass(detailsFocusPosition);
        }
      });

      /*
        When error thrown
      */
      dispatcher.on('error__thrown', function(errorText) {
        console.error(errorText);
      });
    },

    /*
      Setup document events
    */
    documentEvents: function() {

      /*
        When window resizes, determine if it has moved into a new size range
      */
      d3.select(window).on('resize', function() {

        var smallScreenMax = 600;
        var mediumScreenMax = 768;

        var newWindowWidth = window.innerWidth;

        // If the user's window has changed from another size to small
        if (newWindowWidth < smallScreenMax && 
            state.windowSize !== 'small') {

          dispatcher.call('window__changed-to-new-size', this, 'small');
        }
        // If the user's window has changed from another size to medium
        else if (newWindowWidth >= smallScreenMax &&
                 newWindowWidth < mediumScreenMax &&
                 state.windowSize !== 'medium') {

          dispatcher.call('window__changed-to-new-size', this, 'medium');
        }
        // If the user's window has changed from another size to large
        else if (newWindowWidth >= mediumScreenMax &&
                 state.windowSize !== 'large') {

          dispatcher.call('window__changed-to-new-size', this, 'large');
        }
      });

      /*
        When a new suburb is selected
      */
      state.suburbList.selectAll('.featured-suburb-list__suburb').on('click', function() {

        // Only trigger the showing of a suburb's details on click of the whole suburb element
        // when the window is large
        if (state.windowSize === 'large') {

          var suburbElement = d3.select(this);
          var suburb = suburbElement.attr('data-suburb');

          // If the suburb is already in focus, remove that focus and hide the details element
          if (state.suburbInFocus === suburb) {
            dispatcher.call('user__closes-suburb-details');
          }
          // Otherwise, reveals the details pertaining to the selected suburb
          else {
            dispatcher.call('user__changed-suburb-in-focus', this, suburb, suburbElement);
          }
        }
      });

      /*
        When a new suburb is selected (by the selection of its details element)
      */
      state.suburbList.selectAll('.featured-suburb-list__suburb__summary__details').on('click', function() {

        // Only trigger the showing of a suburb's details on click of the details button element
        // when the window is not large (when the window is large, clicks of the button will 
        // 'bubble up' and trigger a click on its containing suburb feature; no need to double-up)
        if (state.windowSize !== 'large') {

          var suburb = d3.select(this).attr('data-suburb');
          var suburbId = '#featured-suburb__' + (suburb.toLowerCase().split(' ').join('-'));
          var suburbElement = d3.select(suburbId);

          // If the suburb is already in focus, remove that focus and hide the details element
          if (state.suburbInFocus === suburb) {
            dispatcher.call('user__closes-suburb-details');
          }
          // Otherwise, reveals the details pertaining to the selected suburb
          else {
            dispatcher.call('user__changed-suburb-in-focus', this, suburb, suburbElement);
          }
        }
      });

    }
  };

  //
  // Draw
  // -----------------------------------------------------------------------------

  var draw = {

    /*
      Draw suburb details

      @param { [Object] } data - Row of objects describing properties within a given suburb, each of
      which has the following propertyies:
        @prop { String } name - Name of property
        @prop { String } address - Address of property
        @prop { String } type - Type of property
        @prop { Number } value - Value of property 
    */  
    suburbDetails: function(data) {

      // Data join
      state.suburbDetailsRows = state.suburbList
        .select('.featured-suburb-list__details__table__body')
        .selectAll('.featured-suburb-list__details__table__body__row')
          .data(data);

      // Update
      state.suburbDetailsRows.each(function(d) {

        var row = d3.select(this);

        row.select('.featured-suburb-list__details__table__name-cell')
            .text(d.name);

        row.select('.featured-suburb-list__details__table__address-cell')
            .text(d.address);

        row.select('.featured-suburb-list__details__table__type-cell')
            .text(d.type);

        row.select('.featured-suburb-list__details__table__value-cell')
            .text('$' + ffx.u.formatNumber(d.value));
      });

      // Enter
      state.suburbDetailsRows.enter().append('div').each(function(d) {

        var row = d3.select(this)
            .classed('featured-suburb-list__details__table__body__row', true);

        row.append('div')
            .classed('featured-suburb-list__details__table__name-cell', true)
            .text(d.name);

        row.append('div')
            .classed('featured-suburb-list__details__table__address-cell', true)
            .text(d.address);

        row.append('div')
            .classed('featured-suburb-list__details__table__type-cell', true)
            .text(d.type);

        row.append('div')
            .classed('featured-suburb-list__details__table__value-cell', true)
            .text('$' + ffx.u.formatNumber(d.value));
      });

      // Exit
      state.suburbDetailsRows.exit().remove();

      // > Calculate and insert total value figure
      var totalValue = 0;
      for (var i = 0; i < data.length; i++) {
        totalValue += data[i].value;
      }

      state.suburbDetails.select('.featured-suburb-list__details__footer__total-value__figure')
          .text(ffx.u.formatNumber(totalValue))

      // > Determine whether to add extra padding to right of details table, to compensate for the
      //   lack of a scroll bar
      state.suburbDetails.classed('is--open', true);

      var listHeight = state.suburbList.select('.featured-suburb-list__details__table__body').property('clientHeight');
      state.suburbDetails.classed('is--not-scrolling-height', listHeight < 150);
    }
  };

  //
  // Utilities
  // -----------------------------------------------------------------------------

  var u = {

    /*
      Get suburb details order position

      @param { Number } suburbOrderPosition - Number denoting index of suburb in list of featured
      suburbs
      @param { String } windowSize - String describing size of user's viewport (either 'small', 
      'medium' or 'large')
    */
    getSuburbDetailsOrderPosition: function(suburbOrderPosition, windowSize) {

      if (windowSize === 'small') {
        return calculateGapOrderPosition(suburbOrderPosition, 4);
      }
      else if (windowSize === 'medium') {
        return calculateGapOrderPosition(suburbOrderPosition, 6);
      }
      else /* (windowSize === 'large') */ {
        return calculateGapOrderPosition(suburbOrderPosition, 10);
      }

      /*
        Calculate gap order position

        @param { Number } suburbPosition - Number denoting index of suburb in list of featured
        suburbs
        @param { Number } intervalSize - Number denoting size of the CSS order length of a given
        row of featured suburb list items, given a particular window size (i.e. when the window is
        large, there are five list items per row, the 'order' value of which increases by two
        with every list item; this makes for a row 'interval size' of 10 (5 items * 2 order positions
        per item = 10 order position interval size))
      */
      function calculateGapOrderPosition(suburbPosition, intervalSize) {
        return Math.ceil(suburbPosition / intervalSize) * intervalSize + 1;
      }
    },

    /*
      Get suburb details focus position

      @param { Number } suburbOrderPosition - Number denoting index of suburb in list of featured
      suburbs
      @param { String } windowSize - String describing size of user's viewport (either 'small', 
      'medium' or 'large')
    */
    getSuburbDetailsFocusPosition: function(suburbOrderPosition, windowSize) {

      var gaplessSuburbOrderPosition = suburbOrderPosition / 2

      if (windowSize === 'small') {
        return Math.ceil((gaplessSuburbOrderPosition - 0.5) % 2);
      }
      else if (windowSize === 'medium') {
        return Math.ceil((gaplessSuburbOrderPosition - 0.5) % 3);
      }
      else /* (windowSize === 'large') */ {
        return Math.ceil((gaplessSuburbOrderPosition - 0.5) % 5);
      }
    },

    /*
      Set suburb details focus class

      @param { Number } suburbFocusPosition - Number denoting index of suburb element being focussed
      upon within its row
    */
    setSuburbDetailsFocusClass: function(suburbFocusPosition) {
      state.suburbDetails.classed('is--focussed-on-1', suburbFocusPosition === 1);
      state.suburbDetails.classed('is--focussed-on-2', suburbFocusPosition === 2);
      state.suburbDetails.classed('is--focussed-on-3', suburbFocusPosition === 3);
      state.suburbDetails.classed('is--focussed-on-4', suburbFocusPosition === 4);
      state.suburbDetails.classed('is--focussed-on-5', suburbFocusPosition === 5);
    }
  };

  //
  // Execute
  // -----------------------------------------------------------------------------

  setup.settings();
  setup.state();
  setup.data();
  setup.dispatcher();
  setup.dispatcherEvents();
  setup.documentEvents();
}

export default featuredSuburbList;