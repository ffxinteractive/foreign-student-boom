/*

  Property search

  February 2, 2018
  Ask sorenfrederiksen@fairfaxmedia.com.au

*/

import * as ffx from './ffx-imports';
import * as d3 from './d3-imports';
import Awesomplete from './lib/awesomplete/awesomplete.js';

window.d3 = d3;

function propertySearch(userSettings) {
  var settings, state, data, dispatcher, autocomplete;

  //
  // Setup
  // -----------------------------------------------------------------------------

  var setup = {

    /*
      Setup settings
    */
    settings: function() {

      // Default settings
      settings = {
        searchId: '#property-search',
        searchInputId: '#property-search__field__input',
        searchFormId: '#property-search__field__form',
        searchResultsId: '#property-search__results',
      };

      // Override default settings, where the user has provided them
      var userSettingsKeys = Object.keys(userSettings);

      for (var i = 0; i < userSettingsKeys.length; i++) {
        settings[userSettingsKeys[i]] = userSettings[userSettingsKeys[i]];
      }
    },

    /*
      Setup suburbs and postcodes data

      Sets up data used to facilitate the autocomplete search function.
    */
    suburbsAndPostcodesData: function() {

      d3.csv('src/data/suburbs-postcodes-and-councils.csv', function(error, data) {
        if (error) {
          dispatcher.call('error__thrown', this, error);
        }
        else {
          window.suburbs = data;
          dispatcher.call('suburbs-and-postcodes-data__loaded', this, data);
        }
      });
    },

    /*
      Setup properties by council data

      Sets up data about properties in each council area - i.e. the results from the autocomplete
      search function.
    */
    propertiesByCouncilData: function() {

      d3.csv('src/data/properties-by-council.csv', process, function(error, data) {
        if (error) {
          dispatcher.call('error__thrown', this, error);
        }
        else {
          window.properties = data;
          dispatcher.call('properties-by-council-data__loaded', this, data);
        }
      });

      function process(d) {
        d.numberOfProperties = +d.numberOfProperties;
        d.value = +d.value;
        return d;
      }
    },

    /*
      Setup state
    */
    state: function() {
      state = {
        simplifiedAutocompleteData: null,
        autocompleteData: null,
        resultsData: null,
        defaultResults: null,
        searchContainer: d3.select(settings.searchId),
        searchInput: d3.select(settings.searchInputId),
        searchForm: d3.select(settings.searchFormId),
        searchResultsNotFound: d3.select('.property-search__results__not-found'),
        searchResultsList: d3.select(settings.searchResultsId),
        searchResultsListItems: null,
        searchInputValue: '',
        searchResultsFilterValue: '',
        autocompleteActiveFirstChar: null
      };
    },

    /*
      Setup autocomplete

      @param { [String] } data - Array of strings describing a suburb or postcode the user can 
      search for
    */
    autocomplete: function(data) {
      if (autocomplete) {
        autocomplete.list = data;
      }
      else {
        autocomplete = new Awesomplete(settings.searchInputId, {
          minChars: 3,
          maxItems: 5,
          list: data,
          sort: function(a, b) { return d3.ascending(a, b); }
        });
      }
    },

    /*
      Setup dispatcher
    */
    dispatcher: function() {
      dispatcher = d3.dispatch(
        'user__submitted-search',
        'user__entered-input-value',
        'suburbs-and-postcodes-data__loaded',
        'properties-by-council-data__loaded',
        'error__thrown'
      );
    },

    /*
      Setup dispatcher events
    */
    dispatcherEvents: function() {

      /*
        Suburbs and postcodes data loaded

        @param { [String] } data - Array of strings describing a suburb or postcode the user can 
        search for
      */
      dispatcher.on('suburbs-and-postcodes-data__loaded', function(data) {
        state.autocompleteData = data;
      });

      /*
        Properties by council data loaded
        
        @param { [Object] } data - Array of objects describing the Catholic Church's portfolio of
        properties within a given area
      */
      dispatcher.on('properties-by-council-data__loaded', function(data) {
        state.resultsData = data;
        state.defaultResults = [];

        data.sort(function(a, b) {
          return d3.descending(a.value, b.value);
        });

        var mobileWidthMax = 601;
        var defaultResultsLength = window.innerWidth < mobileWidthMax ? 3 : 5;

        for (var i = 0; i < defaultResultsLength && i < data.length; i += 1) {
          state.defaultResults.push(data[i]);
        }

        draw.searchResults(state.defaultResults);
      });

      /*
        User entered input value

        @param { String } newValue - Value the user has input
      */
      dispatcher.on('user__entered-input-value', function(newValue) {

        state.searchInputValue = newValue;

        if (state.autocompleteData                           &&
            newValue.length > 0                              &&
            state.autocompleteActiveFirstChar !== newValue[0]) {

          state.autocompleteActiveFirstChar = newValue[0];

          var filteredData = state.autocompleteData.filter(function(d) {
            return u.hasValuesStartingWith(d.locality, d.postcode, state.autocompleteActiveFirstChar);
          });

          state.simplifiedAutocompleteData = d3.map(filteredData, function(d) {
            return d.locality + ' (' + d.postcode + ') - ' + d.council;
          })
          .keys();

          setup.autocomplete(state.simplifiedAutocompleteData);
        }
      });

      /*
        User submitted search

        @param { String } searchValue - Value of query user has searched for
      */
      dispatcher.on('user__submitted-search', function(searchValue) {

        var council = searchValue.split(' - ')[1];

        var activeResults = state.resultsData.filter(function(d) {
          return d.council === council;
        })
        .sort(function(a, b) {
          return d3.ascending(a.council, b.council);
        });

        // If a data point is found, illustrate it
        if (activeResults.length > 0) {
          draw.searchResults(activeResults);
        }
        // Otherwise, if the user appears to have selected a valid council area, but one for which
        // we have no data, input a dummy data point that indicates that is the case
        else if (council && council.length > 0) {
          draw.searchResults([{
            council: council,
            numberOfProperties: 'NA',
            value: 'NA'
          }])
          
          // Ensure input value conforms with results filter value
          state.searchInput.node().value = state.searchResultsFilterValue;
        }
        // If none of the above, indicate no result has been found, likely due to a spelling or
        // other use error
        else {
          draw.searchResults([]);
          draw.noSearchResults(searchValue);
        }
      });

      /*
        Error thrown
      
        @param { String } errorText - String denoting error that has occured.
      */
      dispatcher.on('error__thrown', function(errorText) {
        console.error(errorText);
      });
    },

    /*
      Setup document events
    */

    documentEvents: function() {
      
      /*
        User entered value into search input
      */
      state.searchInput.on('input', function() {
        dispatcher.call('user__entered-input-value', this, d3.select(this).property('value'));
      }); 

      /*
        User has hit backspace in text input
      */
      state.searchInput.on('keydown', function(event) {
        if (event) {
          var key = +event.keyCode || +event.charCode;

          if (key === 8 || key === 46) {
            dispatcher.call('user__entered-input-value', this, d3.select(this).property('value'));
          }
        }
      });

      /*
        User has scrolled autocomplete
      */
      state.searchContainer.select('.awesomplete > ul').on('touchstart', function() {
        d3.select("#" + settings.inputId).node().blur();
      });

      /*
        User selected outside of autocomplete
      */
      d3.select('body').on('click', function() {
        state.searchInput.dispatch('keydown', { detail: { blurInput: true }});
      });

      /*
        User selected inside autocomplete
      */
      state.searchForm.on('click', function() {

        if (state.searchInput.node().value.length > 0) {
          state.searchInput.node().value = '';
          state.searchInput.dispatch('keydown', { detail: { blurInput: true }});
        }
        
        d3.event.stopPropagation();
      });

      /*
        User submits form
      */
      state.searchForm.on('submit', function() {
        d3.event.preventDefault();

        //if (state.searchResultsFilterValue !== '') {
        //  dispatcher.call('user__submitted-search', this, state.searchResultsFilterValue);
        //}
        //else {
          dispatcher.call('user__submitted-search', this, state.searchInputValue);
        //}


        return false;
      });

      /*
        User finishes autocomplete selection
      */
      state.searchForm.on('awesomplete-selectcomplete', function() {
        state.searchResultsFilterValue = state.searchInput.property('value');
        dispatcher.call('user__submitted-search', this, state.searchResultsFilterValue);
      });
    }
  };

  //
  // Draw
  // -----------------------------------------------------------------------------

  var draw = {

    /*
      Draw search results

      @param { [Object] } - Array of objects describing the Catholic church's portfolio of properties
      in a series of local council areas.
    */
    searchResults: function(data) {

      // Data join
      state.searchResultsListItems = state.searchResultsList.selectAll('.property-search__results__item')
          .data(data);

      // Update
      state.searchResultsListItems.each(function(d) {

        var container = d3.select(this)
            .classed('property-search__results__item', true);

        container.select('.property-search__results__item__name')
            .text(d.council);

        container.select('.property-search__results__item__properties-count')
            .text(function(d) {
              if (d.numberOfProperties === 'NA') {
                return 'Data not available'
              }
              else {
                return d.numberOfProperties;
              }
            });

        container.select('.property-search__results__item__properties-value')
            .text(function(d) {
              if (d.value === 'NA') {
                return 'Data not available'
              }
              else {
                return '$' + ffx.u.formatNumber(d.value);
              }
            });
      });

      // Enter
      state.searchResultsListItems.enter().append('article').each(function(d) {

        var container = d3.select(this)
            .classed('property-search__results__item', true);

        container.append('h3')
            .classed('property-search__results__item__name', true)
            .text(d.council);

        container.append('p')
            .classed('property-search__results__item__properties-count-label', true)
            .text('Number of properties');

        container.append('p')
            .classed('property-search__results__item__properties-count', true)
            .text(function(d) {
              if (d.numberOfProperties === 'NA') {
                return 'Data not available'
              }
              else {
                return d.numberOfProperties;
              }
            });

        container.append('p')
            .classed('property-search__results__item__properties-value-label', true)
            .text('Total value');

        container.append('p')
            .classed('property-search__results__item__properties-value', true)
            .text(function(d) {
              if (d.value === 'NA') {
                return 'Data not available'
              }
              else {
                return '$' + ffx.u.formatNumber(d.value);
              }
            });
      });

      // Exit
      state.searchResultsListItems.exit().remove();

      // > Ensure 'results not found' message is hidden, if data was provided
      state.searchResultsNotFound.classed('is--active', !data || data.length === 0);
    },

    /*
      Draw no search results

      @param { String } searchValue - Query string which returned no results
    */
    noSearchResults: function(searchValue) {

      state.searchResultsNotFound.classed('is--active', true);
      state.searchResultsNotFound
        .select('.property-search__results__not-found__message__search-query')
          .text(searchValue);
    }
  };

  //
  // Utilities
  // -----------------------------------------------------------------------------

  var u = {

    /*
      Has values starting with

      @param { String } name - Name of a given suburb
      @param { String } postcode - Postcode of a given suburb
      @param { String } targetLetter - Character we're testing against (i.e. we're checking if either
      the suburb string or postcode string start with this character)
    */
    hasValuesStartingWith: function(name, postcode, targetLetter) {

      if (!name || !postcode || !targetLetter) {
        return false;
      }

      var values = (name + ' ' + postcode).split(' ');

      for (var i = 0; i < values.length; i += 1) {
        if (values[i][0].toLowerCase() === targetLetter.toLowerCase()) {
          return true;
        }
      }

      return false;
    }
  };

  //
  // Execute
  // -----------------------------------------------------------------------------

  setup.settings();
  setup.state();
  setup.autocomplete();
  setup.propertiesByCouncilData();
  setup.suburbsAndPostcodesData();
  setup.dispatcher();
  setup.dispatcherEvents();
  setup.documentEvents();
}

export default propertySearch;