/*
  *-> Catholic Church Property Prices

  January 5, 2018
  Ask Soren Frederiksen if issues arise
*/

import * as d3 from './d3-imports';
import * as ffx from './ffx-imports';
import * as Waypoint from '../../node_modules/waypoints/lib/noframework.waypoints';

import startWaypoints from './waypoint_charts/waypoint.js';
// import propertyGallery from './property-gallery';
import featuredSuburbList from './featured-suburb-list';
import propertySearch from './property-search';
import barChart from './bar-chart';

function catholicChurchFeature(userSettings) {
  var settings;


  //
  // Setup
  // -----------------------------------------------------------------------------

  var setup = {

    settings: function() {

      // Default settings
      settings = {
        loadingIcon: false,
        mastheads: false,
        sharing: false,
        documentEvents: false,
        propertyGallery: false,
        featuredSuburbList: false,
        propertySearch: false,
        barChart: false,
        barChartIsScrollActivated: true,
        autoResizing: false,
        propertyLineChart: false,
        startWaypoints:false
      };

      // Override default settings, where the user has provided them
      var userSettingsKeys = Object.keys(userSettings);

      for (var i = 0; i < userSettingsKeys.length; i++) {
        settings[userSettingsKeys[i]] = userSettings[userSettingsKeys[i]];
      }

      // Override both default and user settings, where url settings are provided
      var urlSettings = ffx.u.getUrlArgumentValues();
      var urlSettingsKeys = Object.keys(urlSettings);

      for (var i = 0; i < urlSettingsKeys.length; i++) {
        settings[urlSettingsKeys[i]] = urlSettings[urlSettingsKeys[i]];
      }
    },

    sharing: function() {
      
      ffx.share.setupMetadata({
        title: "Degrees of risk: inside Sydney's extraordinary international student boom",
        description: 
          "Foreign student numbers are booming and but what impact is it having on Sydney's universities?",
        url: ffx.u.getCurrentPageUrl()
      });


      ffx.share.setupTwitterButtons("social-button__twitter");
      ffx.share.setupFacebookButtons("social-button__facebook");
      ffx.share.setupEmailButtons("social-button__email");
      ffx.share.setupWhatsAppButtons("social-button__whatsapp");
    },

    documentEvents: function() {
      d3.select(window).on('load', function() {
        d3.select('.loader-container').classed('is--active', false);
      })
    }
  };

  //
  // Draw
  // -----------------------------------------------------------------------------

  var draw = {

    mastheads: function() {

      var currentMasthead = u.getCurrentMasthead();
      var mastheadClass;
      var mastheadLink;

      switch (currentMasthead) {
        case 'smh':           mastheadClass = 'is--smh';
                              mastheadLink = 'http://www.smh.com.au/';
                              break;
        case 'theage':        mastheadClass = 'is--the-age';
                              mastheadLink = 'https://www.theage.com.au/';
                              break;
        case 'brisbanetimes': mastheadClass = 'is--bt';
                              mastheadLink = 'https://www.brisbanetimes.com.au/';
                              break;
        case 'watoday':       mastheadClass = 'is--wa-today';
                              mastheadLink = 'http://www.watoday.com.au/';
                              break;
        default:              mastheadClass = 'is--smh';
                              mastheadLink = 'http://www.smh.com.au/';
      }

      d3.select('.header-nav__logo')
          .classed(mastheadClass, true);
          
      d3.select('.header-nav__logo__link')
          .attr('href', mastheadLink);

      d3.select('.footer__header__logo-container__logo')
          .classed(mastheadClass, true);
          
      d3.select('.footer__header__logo-container__logo__link')
          .attr('href', mastheadLink);

      d3.select('.header-nav__back__link')
          .attr('href', mastheadLink);

      d3.select('.header-nav__back')
          .on('click', function() {
            window.location.href = mastheadLink;
          });

      d3.select('.article-header__topic__link')
          .attr('href', mastheadLink + 'national');
    },

    loadingIcon: function() {

      var currentMasthead = u.getCurrentMasthead();
      var loadingIconId;

      switch (currentMasthead) {
        case 'smh':           loadingIconId = '#icon-loading-smh';
                              break;
        case 'theage':        loadingIconId = '#icon-loading-theage';
                              break;
        case 'brisbanetimes': loadingIconId = '#icon-loading-bt';
                              break;
        case 'watoday':       loadingIconId = '#icon-loading-watoday';
                              break;
        case 'canberratimes': loadingIconId = '#icon-loading-ct';
                              break;
        default:              loadingIconId = '#icon-loading-smh';
      }

      d3.select('.loader-icon__svg__image')
          .attr('xlink:href', loadingIconId);
    }
  };

  //
  // Utilities
  // -----------------------------------------------------------------------------

  var u = {

    getCurrentMasthead: function() {

      // If masthead set manually, return that
      if (settings.hasOwnProperty('masthead')) {
        return settings.masthead;
      }
      // Otherwise, determine current masthead using the current url
      else {
        // e.g. 'http://www.smh.com.au/nsw/key-workers-fleeing-sydneys-inner-and-middlering-20180204-h0th7h.html'
        var url = ffx.u.getCurrentPageUrl();

        if (url.indexOf('theage.com.au') > -1) {
          return 'theage';
        }
        else if (url.indexOf('brisbanetimes.com.au') > -1) {
          return 'brisbanetimes';
        }
        else if (url.indexOf('canberratimes.com.au') > -1) {
          return 'canberratimes';
        }
        else if (url.indexOf('watoday.com.au') > -1) {
          return 'watoday';
        }
        else /* if (url.indexOf('smh.com.au') > -1) */ {
          return 'smh';
        }
      }
    },
  };

  //
  // Execute
  // -----------------------------------------------------------------------------

  setup.settings();

  if (settings.loadingIcon) {
    draw.loadingIcon();
  }
  if (settings.mastheads) {
    draw.mastheads();
  }
  if (settings.sharing) {
    setup.sharing();
  }
  if (settings.documentEvents) {
    setup.documentEvents();
  }
  if(settings.startWaypoints){
    startWaypoints();
  }

  if (settings.featuredSuburbList) {
    featuredSuburbList({
      suburbListId: '#featured-suburb-list',
      suburbDetailsId: '#featured-suburb-list__details'
    });
  }
  if (settings.propertySearch) {
    propertySearch({
      searchId: '#property-search',
      searchInputId: '#property-search__field__input',
      searchFormId: '#property-search__field__form'
    });
  }
  if (settings.barChart) {
    barChart({
      barChartContainerId: '#bar-chart',
      isScrollActivated: settings.barChartIsScrollActivated
    });
  }
  if (settings.autoResizing) {
    ffx.iframeMessenger.enableAutoResize();
  }

}

export default catholicChurchFeature;