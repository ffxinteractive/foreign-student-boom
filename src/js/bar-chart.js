/*

  Bar chart

  February 2, 2018
  Ask sorenfrederiksen@fairfaxmedia.com.au

*/

import * as ffx from './ffx-imports';
// import * as d3 from './d3-imports';

function barChart(userSettings) {
  var settings, state, data, scales;

  //
  // Setup
  // -----------------------------------------------------------------------------

  var setup = {

    /*
      Setup settings
    */
    settings: function() {

      // Default settings
      settings = {
        barChartContainerId: '#bar-chart',
        isScrollActivated: true
      };

      // Override default settings, where the user has provided them
      var userSettingsKeys = Object.keys(userSettings);

      for (var i = 0; i < userSettingsKeys.length; i++) {
        settings[userSettingsKeys[i]] = userSettings[userSettingsKeys[i]];
      }
    },

    /*
      Setup state
    */
    state: function() {
      state = {
        chartIsActive: false,
        barChartContainer: d3.select(settings.barChartContainerId)
      };
    },

    /*
      Setup document events
    */
    documentEvents: function() {

      d3.select(window).on('scroll', function(event) { 
        var chartIsInView = u.isBarChartInView();

        if (chartIsInView !== state.chartIsActive) {
          state.barChartContainer.classed('is--active', chartIsInView);
          state.chartIsActive = chartIsInView;
        }
      });
    }
  };

  //
  // Utilities
  // -----------------------------------------------------------------------------

  var u = {

    /*
      Is bar chart in view
    */
    isBarChartInView: function() {

      var windowHeight = u.getWindowHeight();

      var chartTop = state.barChartContainer.node().getBoundingClientRect().top;
      var chartHeight = state.barChartContainer.node().getBoundingClientRect().height;
      var chartCenter = chartTop + (chartHeight / 2);

      var chartMinY = windowHeight * -0.15;
      var chartMaxY = windowHeight * 0.80;

      return chartMinY < chartCenter && chartMaxY > chartCenter;
    },

    /*
      Get window height
    */
    getWindowHeight: function() {
      return window.parent.innerHeight !== window.innerHeight ? window.parent.innerHeight : window.innerHeight;
    }
  };

  //
  // Execute
  // -----------------------------------------------------------------------------

  setup.settings();
  
  setup.state();
  if (settings.isScrollActivated) {
    setup.documentEvents();
  }
  else {
    state.barChartContainer.classed('is--active', true);
    state.chartIsActive = true;
  }
}

export default barChart;