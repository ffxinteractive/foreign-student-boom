
            
		window.onload = function () {
			var domain = 'https//www.smh.com.au';

            var colorsArray = ["#1F77B4", "#FF7F0E", "#b339cc", "#2ca02c", "#c91818" ];
            var colors = {"mac":"#4A90E2", "usyd":"#D0021B", "nsw":"#F5A623", "uts":"#7ED321", "wsu": "#0A1633" };

            var min = 3500, max = 25000;
            var mac, usyd, nsw, uts, wsu, x, y, xAxis, yAxis, uniArray;
    
            var margin = {top: 0, right: 10, bottom: 0, left: 10};
            var yaxisTicks = Math.max(width/45,10);// 5;
            var yAxisOffset = 60;
            if(isMobile){
                yaxisTicks = 6;
            }
            // width = window.innerWidth - margin.left - margin.right,
            var width = $("#chart1").parent().width() - margin.left - margin.right;
            var height = $("#chart1").parent().height() - margin.top - margin.bottom;
    
            var parse = d3.time.format("%b %Y").parse;
    
            // Scales and axes. Note the inverted domain for the y-scale: bigger is up!
            x = d3.time.scale().range([0, width]),
            y = d3.scale.linear().range([height, 50]),
            xAxis = d3.svg.axis().scale(x).ticks(yaxisTicks).tickSize(-height);
            yAxis = d3.svg.axis().scale(y).orient("left");
    
            // An area generator, for the light fill.
            var area = d3.svg.area()
                .interpolate("monotone")
                .x(function(d) { return x(d.date); })
                .y0(height)
                .y1(function(d) { return y(d.count); });
    
            // A line generator, for the dark stroke.
            var line = d3.svg.line()
                .interpolate("monotone")
                .x(function(d) { return x(d.date); })
                .y(function(d) { return y(d.count); });
    
            var svg = d3.select("svg#chart1");
    
    
            d3.csv("data/dataB.csv", type, function(error, data) {
            
             var multipleUniFound= false;
             var u = "";
             for(var i=0; i<data.length; i++){
                 if(u != "" && u != data[i].uni){
                    multipleUniFound= true;
                 }
                 u = data[i].uni;
             }

            // Filter to one uni
            if(!multipleUniFound){
                uniArray = [data];

            }else{
                mac = data.filter(function(d) {
                    return d.uni == "mac";;
                });
        
                usyd = data.filter(function(d) {
                    return d.uni == "usyd";
                });
        
                nsw = data.filter(function(d) {
                    return d.uni == 'nsw';
                });
        
                uts = data.filter(function(d) {
                    return d.uni == 'uts';
                    });
                wsu = data.filter(function(d) {
                return d.uni == 'wsu';
                });

                uniArray = [mac, usyd, nsw, uts, wsu];
            }
    
            // Compute the minimum and maximum date, and the maximum count.
            x.domain([uniArray[0][0].date, uniArray[0][uniArray[0].length - 1].date]);
            
            //y.domain([0, d3.max(values, function(d) { return d.count ; })]).nice();
    
            y.domain([min, max]).nice();
            
            //y.domain([0, d3.max(values, function (d) { return d.count + 10; })])
            //.range([margin.top, height - margin.bottom]); 
    
            // Add an SVG element with the desired dimensions and margin.
            svg.attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
    
            // Add the clip path.
            svg.append("clipPath")
                .attr("id", "clip")
                .append("rect")
                .attr("width", width)
                .attr("height", height);

            // yAxis = d3.svg.axis().scale(y).orient("left");
    
            // Add the x-axis.
            svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(15," + (height+20) + ")")
                .call(xAxis);
    
            // Add the y-axis.
            svg.append("g")
                .attr("class", "y axis")
                .attr("transform", "translate(" + width + ",0)")
                .attr('transform', 'translate(' + [width+yAxisOffset, 0] + ')')
                .call(yAxis);
    
            svg.selectAll('.line')
                .data(uniArray)
                .enter()
                .append('path')
                    .attr('id',  function(d) {
                        return d[0].uni;
                    })
                    .attr('class', 'line')
                    .style('stroke', function(d) {
                        var i = d[0].uni
                        return colors[i];
                   //return colors(Math.random() * 50);
                    })
                    .style('stroke-width', 3)
                    .attr('clip-path', 'url(#clip)')
                    .attr('d', function(d) {
                    return line(d);
                })
    
            /* Add 'curtain' rectangle to hide entire graph */
            var curtain = svg.append('rect')
                .attr('x', -1 * width)
                .attr('y', -1 * (height+10))
                .attr('height', height)
                .attr('width', width)
                .attr('class', 'curtain')
                .attr('transform', 'rotate(180)')
                .style('fill', '#ffffff');
    
                addGridLines();

            //animateLines("in");
            //changeStroke("mac")
  
            });
    
    
            function addGridLines(){
                // Add grid
    
                // svg.append("g")         
                // .attr("class", "grid")
                // .attr("transform", "translate(0," + height + ")")
                // .call(make_x_axis()
                //     .tickSize(-height, 0, 0)
                //     .tickFormat("")
                // )
    
                svg.append("g")         
                .attr("class", "grid")
                .call(make_y_axis()
                    .tickSize(-width, 0, 0)
                    .tickFormat("")
                )
    
            }
    
            function make_x_axis() {        
                return d3.svg.axis()
                    .scale(x)
                     .orient("bottom")
                     .ticks(5)
            }
    
            function make_y_axis() {        
                return d3.svg.axis()
                    .scale(y)
                    .orient("left")
                    // .ticks(5)
            }
    
            function animateLines(n){
                
                if(n == "in"){
                    t= 1500
                    d = 100
                    w = 0;
                }else{
                    d = 0
                    t = 200
                    w = width;
                }
                var t = svg.transition()
                .delay(d)
                .duration(t)
                .ease('linear')
                .each('end', function() {
                  d3.select('line.guide')
                    .transition()
                    .style('opacity', 0)
                    .remove()
                });
              
                t.select('rect.curtain')
                  .attr('width',  w);
                t.select('line.guide')
                  .attr('transform', 'translate(' + w + ', 0)');
            }
    
    
             /* change stroke */
            function changeStroke(s){
    
                d3.selectAll('.line')
                .style('stroke-width', 3)
                .style('stroke', 'grey');
    
                d3.select("#"+s)
                .style('stroke-width', 6)
                .style('stroke', colors[s])
            }
    
            function resetLineColors(){
                d3.selectAll('.line')
                
                .style('stroke', function(d) {
                    var i = d[0].uni
                    return colors[i];
               //return colors(Math.random() * 50);
                })

                .style('stroke-width', 3);

                
            }
    
            // Parse dates and numbers. We assume values are sorted by date.
            function type(d) {
                d.date = parse(d.date);
                d.count = +d.count;
                return d;
            }
    
    
            function changeWidth(n){
                if(n == "in"){
                    w = width;
                }else{
                    w = 0;
                }
    
                d3.select('rect.curtain')
                    .attr('width', w);
            }

            function receiveMessage(e) {
				//if (e.origin !== domain)
					//return;
			 		 if (e.data == 4) {
                         animateLines("in");	
                     }
                     if (e.data == 5) {
                         changeStroke("mac");
                     }
                     if (e.data == 6) {
                         changeStroke("usyd");
                     }
                     if (e.data == 7) {
                        resetLineColors();
                    }
					 if(e.data == 'out'){
						resetLineColors();
						changeWidth('in');
                     }
                     
                     if(e.data == 'outB-A'){
                        resetLineColors();
                     }
			}

        window.addEventListener('message', receiveMessage);
    }