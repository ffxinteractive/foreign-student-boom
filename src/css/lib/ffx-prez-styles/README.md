# [Fairfax Presentation Styles Library](#markdown-header-fairfax-presentation-styles-library)

The **Presentation Styles Library** is a **collection of styles consistent with the Fairfax Blue project's redesign of our online mastheads**. It is intended for use in the production of features, interactives and more by the company's Presentation Development team.

This is the repository for the **distribution** of the library, intended to be cloned and integrated into production projects.

For in-depth documentation or to contribute, see the library's [development repository](https://bitbucket.org/fairfax-prez/ffx-prez-styles). 

It is **highly recommended** that users **do not push updates to this repository directly**, but instead use the distribution-publishing scripts provided in the library's development repository.

## Contents

* [Installation](#markdown-header-installation)

## [Installation](#markdown-header-installation)

### Steps

1. [Installing required third-party software](#markdown-header-1-installing-the-required-third-party-software)
    1. [Node.js and NPM](#markdown-header-a-nodejs-and-npm)
    2. [SASS](#markdown-header-b-sass)
    3. [Node libraries](#markdown-header-c-node-libraries)
        1. [PostCSS and Autoprefixer](#markdown-header-i-postcss-and-autoprefixer)
        2. [Node SASS Chokidar](#markdown-header-ii-sass-chokidar)
        3. [NPM Run All](#markdown-header-iii-npm-run-all)
    4. [Git](#markdown-header-d-git)
2. [Installing the library](#markdown-header-2-installing-the-library)
    1. [Clone this distribution repository](#markdown-header-a-clone-this-distribution-repository)
    2. [Import the library into your SASS](#markdown-header-b-import-the-library-into-your-sass)
3. [Using the library and compiling your SASS](#markdown-header-3-using-the-library-and-compiling-your-sass)
    1. [Using the library](#markdown-header-a-using-the-library)
    2. [Compiling your SASS](#markdown-header-b-compiling-your-sass)

- - - -

### [1. Installing required third-party software](#markdown-header-1-installing-the-required-third-party-software)

To start, we need to make sure you've installed some third-party packages, some of which you've likely already got on your machine.

#### [a. Node.js and NPM](#markdown-header-a-nodejs-and-npm)

Start by making sure you have both Node.js and the Node Package Manager (or NPM) installed. 

(If you don't know what these are, see [here](https://nodejs.org/en/).)

Links for Mac and Windows downloads are [here](https://nodejs.org/en/download/). (NPM now comes packaged with Node.js).

You can confirm both were successfully installed via the command-line ('$' denotes a command-line prompt; don't type it):

```
$ node --version
v7.8.0           # Your numbers will be different; any recent version will do.

$ npm --version
4.2.0            # And again, anything recent will do.
```

#### [b. SASS](#markdown-header-b-sass)

Next, we need to install SASS. (See [here](http://sass-lang.com/) for what it is.)

Follow [these instructions](http://sass-lang.com/install) - the ones on the right - to install SASS.

Once you're done, confirm it was installed succesfully like so:

```
$ sass -v
Sass 3.5.1 (Bleeding Edge)  # How exciting! And messy.
```

#### [c. Node libraries](#markdown-header-c-node-libraries)

These aren't strictly necessary, but they'll make your life much, much easier.

##### [i. PostCSS and Autoprefixer](#markdown-header-i-postcss-and-autoprefixer)

These will save you from having to add browser prefixes ('-webkit') to some of your more cutting-edge CSS styles.

Install both like so:

```
# The '-g' flag is short for 'global'; it means you'll you be able to access these tools from
# anywhere on your computer

$ npm install -g postcss-cli autoprefixer
```

For more information on what these are capable of, see [here](https://github.com/postcss/autoprefixer).

##### [ii. Node SASS Chokidar](#markdown-header-ii-sass-chokidar)

This is a tool we'll use to watch for changes in our SASS files, compiling them to CSS whenever a change occurs.

Install it like so:

```
# Again, we use '-g' here because we want to be able to use this tool from anywhere on our computer

$ npm install -g node-sass-chokidar
```

For more information on this tool, see [here](https://www.npmjs.com/package/node-sass-chokidar).

##### [iii. NPM Run All](#markdown-header-iii-npm-run-all)

This is another command-line tool. It'll make our lives easier by allowing us to run a bunch of our SASS compilation tasks at once.

Install it like so:

```
# And we use '-g' again.

$ npm install -g npm-run-all
```

For more information on NPM Run All, see [here](https://www.npmjs.com/package/npm-run-all).

#### [d. Git](#markdown-header-d-git)

Git is the version control tool that powers sites like Bitbucket.

We're going to use it to download the library's files from Bitbucket.

Mac and Windows downloads of Git can be found [here](https://git-scm.com/downloads).

Once installed, you can verify your installation like so:

```
$ git --version
git version 2.14.2            # Any number should be fine.
```

- - - -

### [2. Installing the library](#markdown-header-2-installing-the-library)

Okay, so now it's time to get the library itself in place.

There are all sorts of ways to structure a project, but this part of the tutorial assumes a structure that looks like this:

```
project
|-- index.html
|-- src
|   └── css
|       └── app.scss            # Your application's SASS styles
|       └── lib                 # Directory for styling libraries
```

To get our 'ffx-prez-styles' library in place, we want to do a few things.

#### [a. Clone this distribution repository](#markdown-header-a-clone-this-distribution-repository)

First, we're going to need to download the library's files. The easiest way to do that is to clone this distribution repository.

Scroll to the top of this page and find the 'Clone' link. On Bitbucket, at present, it appears below the header 'Overview' and next to a dropdown menu with the default value 'HTTPS'.

It should look something like this, but will include your Bitbucket account name in place of 'youraccountname':

```
https://youraccountname@bitbucket.org/fairfax-prez/ffx-prez-styles-distribution.git
```

From the root of your application (in the same directory as 'index.html'), clone the repo like so:

```
$ git clone https://youraccountname@bitbucket.org/fairfax-prez/ffx-prez-styles-distribution.git src/css/lib/ffx-prez-styles
```

This will download the repository's contents, resulting in a project file structure like this:

```
project
|-- index.html
|-- src
|   └── css
|       └── app.scss            # Your application's SASS styles
|       └── lib                 # Directory for styling libraries
|           └── ffx-prez-styles # Contents of the repo
|               └── _ffx-prez-styles.scss
|               └── package.json
|                   └── assets
|                   └── base
|                   └── ... etc.
```

If you've cloned it into the wrong directory, now that it's on your machine, you can just manually copy and paste the repo where you want it.

#### [b. Import the library into your SASS](#markdown-header-b-import-the-library-into-your-sass)

If you haven't already, create a SASS file for your application. (This is where you'll write your application-specific styles, and apply any mix-ins you use from this library.)

This tutorial is going to assume you've called it 'app.scss' and placed it like so:

```
project
|-- index.html
|-- src
|   └── css
|       └── app.scss            # Here!
|       └── lib                 
|           └── ffx-prez-styles 
|               └── _ffx-prez-styles.scss
|               └── package.json
|                   └── assets
|                   └── base
|                   └── ... etc.
```

In your 'app.scss' file, import the presentation styles library like so:

```
// app.scss

@import "lib/ffx-prez-styles/_ffx-prez-styles.scss";
```

With that simple line of code, we can use any of the library's mix-ins, variables and more!

For more on the contents of the library, see [here](https://bitbucket.org/fairfax-prez/ffx-prez-styles).

#### [c. Move the included 'package.json' file to your project's root](#markdown-header-c-move-the-included-packagejson-file-to-your-projects-root)

Included in this distribution repo is an 'example-package.json' file, which we're going to use when compiling our SASS.

It will appear in your project's file structure like so:

```
project
|-- index.html
|-- src
|   └── css
|       └── app.scss            
|       └── lib                 
|           └── ffx-prez-styles 
|               └── _ffx-prez-styles.scss
|               └── example-package.json          # Here!
|                   └── assets
|                   └── base
|                   └── ... etc.
```

But we want to move it to your project's root directory (the same one 'index.html' sits in) and change its name to 'package.json':

```
project
|-- index.html
|── package.json          # Here - and with a changed name!
|-- src
|   └── css
|       └── app.scss            
|       └── lib                 
|           └── ffx-prez-styles 
|               └── _ffx-prez-styles.scss
|                   └── assets
|                   └── base
|                   └── ... etc.
```

Just cut and paste it into the right directory, or include its content in any 'package.json' file your project already has.

- - - -

### [3. Using the library and compiling your SASS](#markdown-header-3-using-the-library-and-compiling-your-sass)

The library's now in place, but how do we use it?

#### [a. Using the library](#markdown-header-a-using-the-library)

Well, now that we've imported the library into our SASS, we can use it like so:

```
// app.scss

@import "lib/ffx-prez-styles/_ffx-prez-styles.scss";

.buttonExample {
  @include button("medium", "dynamic", "solid");
}

```

This will apply the 'button' mix-in detailed [here](https://bitbucket.org/fairfax-prez/ffx-prez-styles) to the 'buttonExample' class.

We can then apply this 'buttonExample' class to our HTML like this:

```
<!-- index.html -->

<!DOCTYPE html>
<html lang="en">
<head>
    ...
    <link href="src/css/app.css" type="text/css" rel="stylesheet"/>
</head>
<body>

  <svg xmlns="http://www.w3.org/2000/svg" style="position:absolute; width: 0; height: 0;">
    <symbol viewBox="0 0 40 40" id="icon-loading">
      <circle class="loading__icon__circle" opacity="0" cx="15.8" cy="15" r="15"></circle>
      <path d="M27.8084006,22.8842813 C29.5771785,20.6011257 30.6299412,17.7353365 
               30.6299412,14.6236613 C30.6299412,9.67647577 27.9688583,5.35081946 
               24,3" class="loading__icon__active-segment"></path>  
    </symbol>
  </svg>
  
  <button class="buttonExample">    <!-- Here! -->
    <span>Button</span>
    <svg 
      viewBox="0 0 40 40" 
      version="1.1" 
      xmlns="http://www.w3.org/2000/svg" 
      xmlns:xlink="http://www.w3.org/1999/xlink" 
      stroke="#096DD2" 
      stroke-width="3" fill="transparent" class="loading">
      <use 
        xmlns:xlink="http://www.w3.org/1999/xlink" 
        xlink:href="#icon-loading" 
        class="loading__icon" 
        transform="translate(6.5 6.5)">
    </svg>
  </button>
  
</body>
</html>

```

And what will happen? Nothing!

We still need to compile our SASS to CSS that our browser can understand!

#### [b. Compiling your SASS](#markdown-header-b-compiling-your-sass)

To compile your project's SASS, open the terminal and navigate to your project's root directory (again, the same one 'index.html' sits in):

```
project
|-- index.html            # Here!
|── package.json          
|-- src
|   └── ... etc.
```

Once there, run this command:

```
$ npm run sass-watcher
```

Just so you know, '**sass-watcher**' refers to a command in your project's 'package.json' file:

```
{
  "name": "ffx-prez-styles",
  "scripts": {
    "build-task:scss-compile": "node-sass-chokidar --source-map true --output-style compressed ./ -o ./",
    "build-task:autoprefixer": "postcss ./*.css --use autoprefixer -d ./",
    "sass:build": "npm-run-all -p build-task:*",
    "sass:watch": "chokidar './**/*.scss' -c 'npm run sass:build'",
    "sass-watcher": "npm-run-all -p sass:*"                         // Here!
  },
  "author": "Soren Frederiksen"
}

```

What it will do is watch for changes to any files with the suffix '.scss' in your project and compile your 'app.scss' file into fresh CSS when they occur.

(It's important that all SASS files other than your main one - 'app.scss', in this case; usually the one you import all of the others into - has an underscore ('\_') at the start of its filename. Only those without an underscore have their contents - including their imported contents - converted to CSS. And that's what we want - a single 'app.css' file that contains all of our application's styles.)

Anyway, you should get output that looks something like this (unless there's an error in your SASS!):

```
Wrote Source Map to /Users/sorenfrederiksen/project/src/css/app.css.map
Wrote 1 CSS files to /Users/sorenfrederiksen/project/src/css/
✔ Finished app.css (278 ms)*
```

Now, your project's file structure should look like this:

```
project
|-- index.html
|── package.json          
|-- src
|   └── css
|       └── app.scss  
|       └── app.css       # CSS our browser will understand!
|       └── app.css.map   # And a '.map' file to help us debug our styles!
|       └── lib                 
|           └── ffx-prez-styles 
|               └── _ffx-prez-styles.scss   # Didn't have a CSS file created for it, because it has
|                   └── assets              # an '_' in front of its name!
|                   └── base
|                   └── ... etc.
```

And our styles should be showing up in the browser!

For a working example of a site setup as described, see the 'ffx-prez-styles-example' repo [here](https://bitbucket.org/fairfax-prez/ffx-prez-styles-example).