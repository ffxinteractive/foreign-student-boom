/*
  Search input

  November 7, 2017
  ask sorenfrederiksen@fairfaxmedia.com.au
  
  A kind of text input specifically designed for search functionality.

  To use a search input, add this to the top of your HTML document (or add the symbol element to 
  an existing svg container):

  ```
  <svg xmlns="http://www.w3.org/2000/svg" style="position:absolute; width: 0; height: 0;">
    <symbol viewBox="0 0 32 32" id="icon-search">
      <path d="M21.08 18.573l10.4 10.4a1.773 1.773 0 1 1-2.508 2.507l-10.4-10.4a11.62 11.62 0 0 1-6.902 2.26C5.225 23.34 0 18.115 0 11.67S5.225 0 11.67 0s11.67 5.225 11.67 11.67c0 2.583-.84 4.97-2.26 6.903zm-1.287-6.903a8.123 8.123 0 1 0-16.247 0 8.123 8.123 0 0 0 16.247 0z" fill="currentColor" fill-rule="evenodd"/>
    </symbol>
  </svg>
  ```
  
  With the above in your HTML document, you can use this style as follows.

  Your SCSS:

  ```
  .yourSearchInputClass {
    @include search-input("standard", "right", "true");
  }
  ```

  Your HTML:
  ```
  <form class="yourSearchInputClass">
    <label>Some invisible search label</label>
    <input type="text" placeholder="Some search placeholder text"></input>
    <button type="submit">
      <svg viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" 
           xmlns:xlink="http://www.w3.org/1999/xlink">
        <use 
          xmlns:xlink="http://www.w3.org/1999/xlink" 
          xlink:href="#icon-search" >
      </svg>
    </button>
  </form>
  ```

  Your HTML when using autocomplete:
  ```
  <form class="yourSearchInputClass">
    <label>Some invisible search label</label>
    <input 
      type="text" 
      placeholder="Some search placeholder text" 
      id="text-input-with-autocomplete"/>
    <button type="submit">
      <svg viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" 
           xmlns:xlink="http://www.w3.org/1999/xlink">
        <use 
          xmlns:xlink="http://www.w3.org/1999/xlink" 
          xlink:href="#icon-search" >
      </svg>
    </button>
  </form>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/awesomplete/1.1.2/awesomplete.js"></script>
  <script>
    var data = ["Derpit", "Burpit", "Fartit", "McGardit", "Lardit", "Dingo", "Brunswick", "Brunton", "Brunsby"]

    var auto = new Awesomplete("#text-input-with-autocomplete", {
      minChars: 2,
      maxItems: 5,
      list: data
    });
  </script>
  ```
*/
@import "../modules/_text-input-autocomplete";

// *---> Private mix-ins

@mixin _search-input(
  $color, 
  $border, 
  $focus-color, 
  $focus-border, 
  $font-size, 
  $background,
  $label-color, 
  $label-font-size,
  $icon-position,
  $outline: initial) {
  
  @include _text-input-without-validation-error(
    $color, 
    $border, 
    $focus-color, 
    $focus-border, 
    $font-size, 
    $background,
    $label-color, 
    $label-font-size,
    $outline
  );

  $new-font-size: to-unit(rem, $font-size);
  $proportion-of-default-size: to-num($new-font-size / 1rem);

  position: relative;

  // Padding for search submit button
  input[type=text] {
    @if ($icon-position == "right") {
      padding-right: (45px * $proportion-of-default-size);
    }
    @else { /* $icon-position == "left" */
      padding-left: (45px * $proportion-of-default-size);
    }
  }

  // Search submit button
  button {
    position: absolute;

    height: 55%;
    padding: 0;
    border: 0;
    background: transparent;
    cursor: pointer;

    @if ($icon-position == "right") {
      top: (10px * $proportion-of-default-size);
      right: (15px * $proportion-of-default-size);
    }
    @else { /* $icon-position == "left" */
      top: (10px * $proportion-of-default-size);
      left: (15px * $proportion-of-default-size);
    }

    svg {
      height: 100%;
      color: $color;
    }
  }

  // Deactivated
  &.is--deactivated {
    button {
      cursor: initial;
    }
  }

  // Hide label, but keep for screen readers, etc.
  label {
    height: 0;
    width: 0;
    padding: 0;
    margin: 0;
    overflow: hidden;
  }
}

// *---> Public mix-ins

/*
  Search input

  @param { String } style - String denoting style of text input
  @param { String } icon-position - String denoting position of search icon
  @param { String } autocomplete - String determining whether or search input will include 
  autocomplete
*/

@mixin search-input($style: "standard", $icon-position: "right", $autocomplete: "false") {
  
  @if ($style == "standard") {
    @include _search-input(
      $color__black--medium,
      1px solid $color__gray--medium,
      $color__black--medium,
      1px solid $color__blue--medium-2,
      1rem,
      #fff,
      $color__black--medium,
      0.9375rem,
      $icon-position,
      initial
    );
  }

  @if ($style == "standard--dark") {
    @include _search-input(
      $color__black--medium,
      1px solid $color__brand,
      $color__black--medium,
      1px solid $color__blue--medium-2,
      1rem,
      #fff,
      #fff,
      0.9375rem,
      $icon-position,
      initial
    );
  }

  @if ($autocomplete == "true") {
    .awesomplete {
      @include autocomplete();
    }
  }
}

/*
  Custom search input

  @param { Color } border-color - Colour of the input's border
  @param { Color } focus-border-color - Colour of the input's border in focus state
  @param { Color } color - Colour of the input's text
  @param { Number with unit } font-size - Size of input text (label is sized at 93.75% of this)
  @param { Color } background - Colour of input's background
  @param { Color } outline - Colour of outline shown around input when it is in focus
*/

@mixin search-input-custom(
  $border-color: $color__gray--medium, 
  $focus-border-color: $color__blue--medium-2,
  $color: $color__black--medium,
  $font-size: 1rem,
  $background: #fff,
  $icon-position: "right",
  $outline: initial,
  $autocomplete: "false") {

  @include _search-input(
    $color,
    1px solid $border-color,
    $color,
    1px solid $focus-border-color,
    $font-size, 
    $background,
    $color,
    $font-size * 0.9375,
    $icon-position,
    $outline
  );

  @if ($autocomplete == "true") {
    .awesomplete {
      @include _autocomplete(
        $font-size,
        1px solid $color__blue--medium-2,
        $color__black--medium,
        $color__black--medium,
        $background,
        darken($background, 10),
        darken($background, 10),
        darken($background, 20)
      );
    }
  }
}