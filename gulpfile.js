// Load plugins
var gulp = require('gulp'),
    usemin = require('gulp-usemin');
    runSequence = require('gulp-run-sequence'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    cache = require('gulp-cache'),
    del = require('del'),
    zip = require('gulp-zip');

//usemin

gulp.task('usemin', function() {
  return gulp.src('*.html')
      .pipe(usemin({
          //jsLibs: ['concat'],
          jsLibs: [],
          cssLibs: []
      }))
      .pipe(gulp.dest('build'));
});


// version rename file 

gulp.task('rename', function() {
  return gulp.src(['build/js/app.min.js'])
    .pipe(filever())
    .pipe(gulp.dest('build/js'));
});
 


//copy folders

gulp.task('copyfolders', function() {

  gulp.src(['*.html']).pipe(gulp.dest('build'));
  

  gulp.src(['src/css/lib/ffx-prez-styles/assets/**/*']).pipe(gulp.dest('build/src/css/lib/ffx-prez-styles/assets'));
  

	gulp.src(['src/chart/**/*']).pipe(gulp.dest('build/src/chart'));
	gulp.src(['src/img/**/*']).pipe(gulp.dest('build/src/img'));
	gulp.src(['src/data/**/*']).pipe(gulp.dest('build/src/data'));
	gulp.src(['src/css/main.css']).pipe(gulp.dest('build/src/css'));
	gulp.src(['src/css/ad.css']).pipe(gulp.dest('build/src/css'));
	gulp.src(['src/css/waypoint_charts/**/*']).pipe(gulp.dest('build/src/css/waypoint_charts'));
	gulp.src(['src/js/waypoint_charts/**/*']).pipe(gulp.dest('build/src/js/waypoint_charts'));
	gulp.src(['src/js/browser.js']).pipe(gulp.dest('build/src/js'));
	gulp.src(['src/js/isMobile.js']).pipe(gulp.dest('build/src/js'));
	gulp.src(['src/js/bundle.min.js']).pipe(gulp.dest('build/src/js'));
	gulp.src(['src/js/bundle.min.js.map']).pipe(gulp.dest('build/src/js'));
	
});


// processImages
gulp.task('processImages', function() {
  return gulp.src('src/images/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('build/images'))
    .pipe(notify({ message: 'Images task complete' }));
});

// Clean
gulp.task('clean', function() {
  return del(['build/*']);
});

var zipName = "v9";
gulp.task('zipup', function(){
    return gulp.src('build/**/*')
        .pipe(zip(zipName+'.zip'))
        .pipe(gulp.dest('build'));
});


// Default task
gulp.task('default',  function() {
    runSequence(
        ['clean'],['copyfolders'], 'zipup'
    )
  //gulp.start( ['usemin', 'processImages',  'copyfolders'], 'zipup');
});

