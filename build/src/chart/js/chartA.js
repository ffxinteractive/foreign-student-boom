		window.onload = function () {
            
            var debugging = false;
			var domain = 'https//www.smh.com.au';

            var colorsArray = ["#1F77B4", "#FF7F0E", "#b339cc", "#2ca02c", "#c91818" ];
            var colors = {"mac":"#4A90E2", "usyd":"#D0021B", "nsw":"#F5A623", "uts":"#7ED321", "wsu": "#0A1633" };
            var inactiveColr = "#717171";

            var min = 40000, max = 75861, mac, usyd, nsw, uts, wsu, x, y, xAxis, yAxis, uniArray, width, height, parse, svg;
    
            var margin = {top: 0, right: 10, bottom: 0, left: 10};
            var yaxisTicks = Math.max(width/45,10);// 5;
            var yAxisOffset = 60;

            function redraw(){

                if(svg){
                    svg.selectAll("*").remove();
                }

                if(isMobile){
                    yaxisTicks = 6;
                }

                width = $("#chart1").parent().width() - margin.left - margin.right;
                height = $("#chart1").parent().height() - margin.top - margin.bottom;
        
                parse = d3.time.format("%b %Y").parse;
        
                // Scales and axes. Note the inverted domain for the y-scale: bigger is up!
                x = d3.time.scale().range([0, width]),
                y = d3.scale.linear().range([height, 50]),
                
                xAxis = d3.svg.axis().scale(x).ticks(yaxisTicks).tickSize(-height);

                yAxis = d3.svg.axis().scale(y).orient("left");
        
                // An area generator, for the light fill.
                var area = d3.svg.area()
                    .interpolate("monotone")
                    .x(function(d) { return x(d.date); })
                    .y0(height)
                    .y1(function(d) { return y(d.count); });
        
                // A line generator, for the dark stroke.
                var line = d3.svg.line()
                    .interpolate("monotone")
                    .x(function(d) { return x(d.date); })
                    .y(function(d) { return y(d.count); });


                // A line generator, for the dark stroke.
                var lineB = d3.svg.line()
                .interpolate("monotone")
                .x(function(d) { return x(d.date); })
                .y(function(d) { return y(d.count); });

                // A line generator, for the dark stroke.
                var lineC = d3.svg.line()
                .interpolate("monotone")
                .x(function(d) { return x(d.date); })
                .y(function(d) { return y(d.count); });
        
                svg = d3.select("svg#chart1");
        
        
                d3.csv("data/dataA.csv", type, function(error, data) {

                    var multipleUniFound= false;
                    var u = "";
                    for(var i=0; i< data.length; i++){
                        if(u != "" && u != data[i].uni){
                            multipleUniFound= true;
                        }
                        u = data[i].uni;
                    }
                    // Filter to one uni
                    if(!multipleUniFound){
                        
                        mac = data.filter(function(d) {
                            return d.uni == "all";;
                        });
                        
                        uniArray = [mac];

                    }else{
                        mac = data.filter(function(d) {
                            return d.uni == "mac";;
                        });
                
                        usyd = data.filter(function(d) {
                            return d.uni == "usyd";
                        });
                
                        nsw = data.filter(function(d) {
                            return d.uni == 'nsw';
                        });
                
                        uts = data.filter(function(d) {
                            return d.uni == 'uts';
                            });
                        wsu = data.filter(function(d) {
                        return d.uni == 'wsu';
                        });

                        uniArray = [mac, usyd, nsw, uts, wsu];
                    }

                    // Compute the minimum and maximum date, and the maximum count.

                    x.domain([uniArray[0][0].date, uniArray[0][uniArray[0].length - 1].date]);//.nice(8);
                    //y.domain([0, d3.max(values, function(d) { return d.count ; })]).nice();
            
                    y.domain([min, max]).nice();
                    
                    //y.domain([0, d3.max(values, function (d) { return d.count + 10; })])
                    //.range([margin.top, height - margin.bottom]); 
            
            
                    // Add an SVG element with the desired dimensions and margin.
                    //var svg = d3.select("svg#chart1")
                    svg.attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
            
                    // Add the clip path.
                    svg.append("clipPath")
                        .attr("id", "clip")
                        .append("rect")
                        .attr("width", width)
                        .attr("height", height);
                    
                    // yAxis = d3.svg.axis().scale(y).orient("left");
            
                    // Add the x-axis.
                    svg.append("g")
                        .attr("class", "x axis")
                        .attr("transform", "translate(15," + (height+20) + ")")
                        .call(xAxis);

                    
                    //d3.select(d3.selectAll(".tick")[0][14]).attr("visibility","hidden");
                   // d3.select(d3.selectAll(".tick")[0][7]).text("2017");
            
                
                    // Add the y-axis.
                    svg.append("g")
                        .attr("class", "y axis")
                        .attr("transform", "translate(" + width + ",0)")
                        .attr('transform', 'translate(' + [width+yAxisOffset, 0] + ')')
                        .call(yAxis);
                    
            
                    svg.selectAll('.line')
                        .data(uniArray)
                        .enter()
                        .append('path')
                        .attr('id',  function(d) {
                            return d[0].uni;
                        })
                        .attr('class', 'line')
                        .style('stroke',"#4A90E2")
                        .style('stroke-width',3)
                        .attr('clip-path', 'url(#clip)')
                        .attr('d', function(d) {
                            return line(d);
                        })

                    
                    var arrA=[[]] , arrB=[[]] ;

                    var len = uniArray[0].length/2;
                    for(var k=0; k<uniArray[0].length; k++){
                        if(k <= uniArray[0].length * 0.80 ){

                            arrA[0].push(uniArray[0][k]);
                        }
                        if(k >= uniArray[0].length * 0.75 ){

                            arrB[0].push(uniArray[0][k]);
                        }
                    }
                    
                    svg.selectAll('.lineB')
                        .data(arrA)
                        .enter()
                        .append('path')
                        .attr('id',  function(d) {
                            return d[0].uni;
                        })
                        .attr('class', 'lineB')
                        .style('stroke','#4A90E2')
                        .style('stroke-width', 6)
                        .attr('clip-path', 'url(#clip)')
                        .attr('d', function(d) {
                            return lineB(d);
                        });

                    svg.selectAll('.lineC')
                        .data(arrB)
                        .enter()
                        .append('path')
                        .attr('id',  function(d) {
                            return d[0].uni;
                        })
                        .attr('class', 'lineC')
                        .style('stroke','#4A90E2')
                        .style('stroke-width', 6)
                        .attr('clip-path', 'url(#clip)')
                        .attr('d', function(d) {
                            return lineC(d);
                        });

                    
                    $(".lineB").hide();
                    $(".lineC").hide();
                
                    /* Add 'curtain' rectangle to hide entire graph */
                    var curtain = svg.append('rect')
                        .attr('x', -1 * width)
                        .attr('y', -1 * (height+10))
                        .attr('height', height)
                        .attr('width', width)
                        .attr('class', 'curtain')
                        .attr('transform', 'rotate(180)')
                        .style('fill', '#ffffff');
            
                    
                    addGridLines();
                    
                    if(debugging){
                        animateLines("in");
                        //changeStroke("mac")
                    }
    
                });
            }
    
    
            function addGridLines(){
                // Add grid
    
                // svg.append("g")         
                // .attr("class", "grid")
                // .attr("transform", "translate(0," + height + ")")
                // .call(make_x_axis()
                //     .tickSize(-height, 0, 0)
                //     .tickFormat("")
                // )

                svg.append("g")         
                .attr("class", "grid")
                .call(make_y_axis()
                    .tickSize(-width, 0, 0)
                    .tickFormat("")
                )
            }
    
            function make_x_axis() {        
                return d3.svg.axis()
                    .scale(x)
                     .orient("bottom")
                     .ticks(5)
            }
    
            function make_y_axis() {        
                return d3.svg.axis()
                    .scale(y)
                    .orient("left")
                    // .ticks(5)
            }
    
            function animateLines(n){
                
                if(n == "in"){
                    t= 1500
                    d = 100
                    w = 0;
                }else{
                    d = 0
                    t = 200
                    w = width;
                }
                var t = svg.transition()
                .delay(d)
                .duration(t)
                .ease('linear')
                .each('end', function() {
                  d3.select('line.guide')
                    .transition()
                    .style('opacity', 0)
                    .remove()
                });
              
                t.select('rect.curtain')
                  .attr('width',  w);
                t.select('line.guide')
                  .attr('transform', 'translate(' + w + ', 0)');
    
                 // //console.log(n)
            }


             /* change stroke */
             function changeStrokeColor(){
    
                d3.selectAll('.line')
                .style('stroke-width', 3)
                .style('stroke', '#717171');
             }
    
    
             /* change stroke */
            function changeStroke(s){
    
                d3.selectAll('.line')
                .style('stroke-width', 3)
                .style('stroke', '#717171');
    
                d3.select("#"+s)
                .style('stroke-width', 6)
                .style('stroke', colors[s])
    
            }
    
            function resetLineColors(){
                d3.selectAll('.line')
    
                .style('stroke', function(d, i) {
                    ////console.log(i)
                    return colorsArray[i];
                })
                .style('stroke-width', 3);

            
            }
    
            function type(d) {
                d.date = parse(d.date);
                d.count = +d.count;
                return d;
            }
    
            function changeWidth(n){

                d3.selectAll('.line')
                .style('stroke-width', 3)
                .style('stroke', '#4A90E2');

                if(n == "in"){
                    w = width;
                }else{
                    w = 0;
                }
    
                d3.select('rect.curtain')
                    .attr('width', w);
            }



            function receiveMessage(e) {
                //if (e.origin !== domain)
                //return;
    
                if (e.data == 0) {
                    animateLines("in");	
                }
    
                if (e.data == 2) {
                    //changeStroke("mac");
                    $('#tt1').css({opacity: 1})
                   // $('#tt2').css({top: '26%', opacity: 0});
                    
                    changeStrokeColor();
                    $(".lineB").show();
                    $(".lineC").hide();
                }
    
                if (e.data == 3) {

                    if(window.innerWidth < 600){

                    }else{

                    }
                   // $('#tt1').css({left:'16%', opacity: 0});
                    $('#tt2').css({ opacity: 1})
                    
                    changeStrokeColor();
                    $(".lineB").hide();
                    $(".lineC").show();
                }
                    
                if(e.data == 'out'){
                
                    // $('#tt1').css({left:'16%', opacity: 0});
                    // $('#tt2').css({top: '27%', opacity: 0});

                    $('#tt1').css({ opacity: 0});
                    $('#tt2').css({opacity: 0});

                    $(".lineB").hide();
                    $(".lineC").hide();
                    changeWidth('in');
                }

                if(e.data == 'outA'){
                
                    //console.log("outA")
                    $('#tt1').css({ opacity: 0});
                    $('#tt2').css({opacity: 0});

                    $(".lineB").hide();
                    $(".lineC").hide();
                    changeWidth('stay');
                }
            }
             // Draw for the first time to initialize.
            redraw();

            // Redraw based on the new size whenever the browser window is resized.

            if(!isMobile){
               //window.addEventListener("resize", redraw);
            }
            window.addEventListener('message', receiveMessage);


            var obj = document.getElementById("tt1");
            var h = window.parent.innerHeight ;
            var w = window.parent.innerWidth ;
            
            var cl = ""
            
       
       function addClass(){
            if(w > 748 && h > 800 && h <= 1099){
               cl = "tt-800";
               obj.classList.add(cl);
               return;
            }

            if(w > 748 && h > 1100){

                cl = "tt-1100";
                obj.classList.add(cl);

                return;
    
            }
        }

        addClass();


            



		}
           