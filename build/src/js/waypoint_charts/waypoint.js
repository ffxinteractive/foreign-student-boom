
import Waypoint from '../lib/noframework.waypoints.min.js';

function startWaypoints(userSettings) {

    //var domain = 'http://localhost:2222';
    var domain = '*';
    // helper function so we can map over dom selection
    function selectionToArray(selection) {
        var len = selection.length
        var result = []
        for (var i = 0; i < len; i++) {
            result.push(selection[i])
        }
        return result
    }

    var setup = {

    /*
        Setup settings
    */
    
    waypoints: function(wp, div) {
        // select elements
        //var graphicEl = document.querySelector('.graphic')
        var graphicEl = document.getElementById(wp);

        var graphicVisEl = graphicEl.querySelector('.graphic__vis')
        var triggerEls = selectionToArray(graphicEl.querySelectorAll('.trigger'))

        // viewport height
        var viewportHeight = window.innerHeight
        var halfViewportHeight = Math.floor(viewportHeight / 2)
        var requestAnimation, linechart, scrollDirection;

        // handle the fixed/static position of grahpic
        var toggle = function(fixed, bottom) {
            if (fixed) graphicVisEl.classList.add('is-fixed')
            else graphicVisEl.classList.remove('is-fixed')

            if (bottom) graphicVisEl.classList.add('is-bottom')
            else graphicVisEl.classList.remove('is-bottom')
        }

        // setup a waypoint trigger for each trigger element
        var waypoints = triggerEls.map(function(el) {
            
            // get the step, cast as number					
            var step = +el.getAttribute('data-step')

            return new Waypoint({
                element: el, // our trigger element
                handler: function(direction) {
                    // if the direction is down then we use that number,
                    // else, we want to trigger the previous one
                    var nextStep = direction === 'down' ? step : Math.max(0, step - 1)
                    ////console.log(nextStep);
                    scrollDirection = direction;

                    requestAnimation = window.requestAnimationFrame(scrollPlay);	

                    linechart = document.getElementById(div).contentWindow;
                    if (nextStep == 0) {
                        linechart.postMessage('0', domain);
                    }
                    if (nextStep == 2) {
                        linechart.postMessage('2', domain);
                    }
                    if (nextStep == 3) {
                        linechart.postMessage('3', domain);
                    }
                    if (nextStep == 4) {
                        linechart.postMessage('4', domain);
                    }
                    if (nextStep == 5) {
                        linechart.postMessage('5', domain);
                    }
            
                    if (nextStep == 6) {
                        linechart.postMessage('6', domain);
                    }
                    if (nextStep == 7) {
                        linechart.postMessage('7', domain);
                    }
                    if (nextStep == 8) {
                        linechart.postMessage('8', domain);
                    }
            
                    if (nextStep == 9) {
                        linechart.postMessage('9', domain);
                    }
                },
                offset: '50%',  // trigger halfway up the viewport
            })
        })
    ,

        scrollPlay=function(){  
            var frameNumber  = window.pageYOffset;
            
            ////console.log(frameNumber)
            if(frameNumber < 500 ){
               linechart.postMessage('out', domain);
            }
            if(frameNumber < 5455 &&  scrollDirection == 'up')
            //if(frameNumber >= 2500 && frameNumber < 2558 ){
                linechart.postMessage('outB-A', domain);
             //}
            window.requestAnimationFrame(scrollPlay);
        };

            var enterWaypoint = new Waypoint({
                element: graphicEl,
                handler: function(direction) {
                    var fixed = direction === 'down'
                    var bottom = false
                    toggle(fixed, bottom)
                },
            })

            var exitWaypoint = new Waypoint({
                element: graphicEl,
                handler: function(direction) {
                    var fixed = direction === 'up'
                    var bottom = !fixed
                    toggle(fixed, bottom)
                },
                offset: 'bottom-in-view', // waypoints convenience instead of a calculation
            })
        //}
    }

}

    setup.waypoints('waypointA','linechartA');
    setup.waypoints('waypointB','linechartB');

}

export default startWaypoints;